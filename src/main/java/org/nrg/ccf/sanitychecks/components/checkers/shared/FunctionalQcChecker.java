package org.nrg.ccf.sanitychecks.components.checkers.shared;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.sanitychecks.constants.CheckResult;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.inter.SanityCheckerI;
import org.nrg.xdat.model.XnatAbstractstatisticsI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.model.XnatQcassessmentdataScanI;
import org.nrg.xdat.model.XnatStatisticsdataAdditionalstatisticsI;
import org.nrg.xdat.om.XnatQcassessmentdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.XnatStatisticsdata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class FunctionalQcChecker implements SanityCheckerI {
	
	public static final String FUNCTIONAL_QC_CHECKS_SHORT_NAME = "func-qc";
	public static final String FUNCTIONAL_QC_CHECKS_LONG_NAME = "Level 2 Functional QC";
	public final SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
	
	public static final String 
			TFMRI_RGX = "(?i)^tfMRI.*", 
			RFMRI_RGX = "(?i)^rfMRI.*", 
			DMRI_RGX = "(?i)^dMRI.*",
			SBREF_RGX = "(?i)^.*_SBRef$" 
			;
	
	@Override
	public List<SanityCheck> doChecks(String projectId, XnatImagesessiondataI imageSession, UserI user) {
		
		log.debug("Run FunctionalQcChecker checks.");
		
		final ArrayList<SanityCheck> checkList = new ArrayList<>();
		
		final CriteriaCollection valCC = new CriteriaCollection("AND");
	      	valCC.addClause("xnat:qcAssessmentData.imageSession_ID",imageSession.getId());
	      	valCC.addClause("xnat:qcAssessmentData.project",projectId);
		final List<XnatQcassessmentdata> currDatas = XnatQcassessmentdata.getXnatQcassessmentdatasByField(valCC, user, false);
		
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			
			final String scanId = scan.getId();
			final String currSeriesDesc = scan.getSeriesDescription();
			if (currSeriesDesc == null || currSeriesDesc.matches(SBREF_RGX) || currSeriesDesc.toUpperCase().contains("PHYSIO") ||
					!(currSeriesDesc.matches(RFMRI_RGX) || currSeriesDesc.matches(TFMRI_RGX))) {
				continue;
			}
			final SanityCheck scanCheck = new SanityCheck(scan);
			scanCheck.setCheckLongName(FUNCTIONAL_QC_CHECKS_LONG_NAME);
			scanCheck.setCheckShortName(FUNCTIONAL_QC_CHECKS_SHORT_NAME);
			final CheckResult result = doFunctionalQcChecks(scan, user, currDatas);
			if (result == null) {
				continue;
			}
			final Outcome outcome = result.getOutcome();
			log.debug("Check scan failure status (SCAN=" + scanId + ",OUTCOME=" + outcome + ")");
			scanCheck.setOutcome(outcome);
			if (outcome.equals(Outcome.FAILED)) {
				scanCheck.setDescriptionOfFailure(result.getDescriptionOfFailure());
			}
			checkList.add(scanCheck);
			
		}
		return checkList;
	}

	private CheckResult doFunctionalQcChecks(XnatImagescandataI scan, UserI user, List<XnatQcassessmentdata> currDatas) {
		final CheckResult result = new CheckResult(Outcome.PASSED);
		final QcAssessmentAndScan qcAS = getQcassessmentForScan(scan, currDatas);
		if (qcAS == null) {
			result.setOutcome(Outcome.FAILED);
			result.setDescriptionOfFailure("Missing Level 2 QC");
			return result;
		}
		final XnatQcassessmentdataScanI qcScan = qcAS.getQcScan();
		final XnatQcassessmentdata qcAssessment = qcAS.getQcAssessment();
		final XnatAbstractstatisticsI scanStats = qcScan.getScanstatistics();
		if (scanStats instanceof XnatStatisticsdata) {
			XnatStatisticsdata stats = (XnatStatisticsdata)scanStats;
			for (final XnatStatisticsdataAdditionalstatisticsI addStat : stats.getAdditionalstatistics()) {
				if (addStat.getAdditionalstatistics() == null) { 
					result.setOutcome(Outcome.FAILED);
					result.setDescriptionOfFailure("Empty values in QC assessment");
					return result;
				}
			}
		}
		final Date qcDate = getQcDate(qcAssessment);
		final Date niftiDate = getNiftiDate(scan);
		if (qcDate !=null && niftiDate != null) {
			if (qcDate.before(niftiDate)) {
				result.setOutcome(Outcome.FAILED);
				result.setDescriptionOfFailure("Functional QC assessment is older than NIFTI");
				return result;
			}
			
		}
		return result;
	}

	private QcAssessmentAndScan getQcassessmentForScan(XnatImagescandataI scan, List<XnatQcassessmentdata> currDatas) {
		for (final XnatQcassessmentdata qcAssessment : currDatas) {
			for (final XnatQcassessmentdataScanI qcScan : qcAssessment.getScans_scan()) {
				if (qcScan.getId().equals(scan.getId())) {
					return new QcAssessmentAndScan(qcAssessment, qcScan);
				}
			}
		}
		return null;
	}
	
	@Getter
	@Setter
	@AllArgsConstructor
	private class QcAssessmentAndScan {
		
		private XnatQcassessmentdata qcAssessment; 
		private XnatQcassessmentdataScanI qcScan; 
		
	}
	
	private Date getQcDate(XnatQcassessmentdata qcdata) {
		if (qcdata.getDate() == null || (!(qcdata.getDate() instanceof Date))) {
			return null;
		}
		Date qcDate = (Date)qcdata.getDate();
		// Let's use insert date for more accurate date (including time), unless the date value doesn't match (re-upload of old qcdata).
		final Date insertDate = qcdata.getInsertDate();
		if (fmt.format(qcDate).equals(fmt.format(insertDate))) {
			qcDate = insertDate;
		}
		return qcDate;
	}

	private Date getNiftiDate(XnatImagescandataI scan) {
		final XnatResourcecatalog niftiResource = ResourceUtils.getResource(scan, CommonConstants.NIFTI_RESOURCE);
		if (niftiResource == null) {
			return null;
		}
		Long niftiDate = ResourceUtils.getEarliestFileModTime(niftiResource);
		if (niftiDate == null) {
			return null;
		}
		return new Date(niftiDate);
	}



}
