package org.nrg.ccf.sanitychecks.components.abst;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.ccf.sanitychecks.abst.AbstractNiftiCheckerSanityChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.ResourceVerificationChecker;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.exception.SessionBuildingException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.pojo.ProcessingResults;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.ccf.sessionbuilding.utils.CcfSessionBuildingUtils;
import org.nrg.xdat.model.SanSanitychecksCheckI;
import org.nrg.xdat.model.SanSanitychecksScanChecksCheckI;
import org.nrg.xdat.model.SanSanitychecksScanChecksI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.SanSanitychecks;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.springframework.core.style.ToStringCreator;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public abstract class AbstractCcfStagingSanityChecker extends AbstractNiftiCheckerSanityChecker {
	
   	public static final String SESSION_BUILDING_CHECK_SHORT_NAME = "session_building_consistency";
   	public static final String SESSION_BUILDING_CHECK_LONG_NAME = "Session Rebuild Consistency Check";
   	
   	public String[] SOURCE_PROJECTS;
   	
   	public abstract void initializeSourceProjectValue();
   	public abstract String getSelectionCriteriaFromSession(XnatImagesessiondataI session);
   	
	protected CcfReleaseRulesUtils _releaseRulesUtils;
	protected ResourceVerificationChecker _resourceVerificationChecker;

	public AbstractCcfStagingSanityChecker(ResourceVerificationChecker resourceVerificationChecker, CcfReleaseRulesUtils releaseRulesUtils) {
		super();
		_releaseRulesUtils = releaseRulesUtils;
		_resourceVerificationChecker = resourceVerificationChecker;
   		initializeSourceProjectValue();
	}
   	
	public abstract void doNonStagingNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user, 
			List<SanityCheck> checkList) throws SanityChecksException;
	
	@Override
	public void doNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user, 
			List<SanityCheck> checkList) throws SanityChecksException {
		
		doNonStagingNonNiftiChecks(projectId, imageSession, user, checkList);
		
		checkList.addAll(_resourceVerificationChecker.doChecks(projectId, imageSession, user));
		
		doSessionBuildingCheck(imageSession, checkList, user);
		
		autoAcceptFailedChecksThatWereAcceptedInIntake(projectId, imageSession, checkList, user);
		
	}
	
	protected void autoAcceptFailedChecksThatWereAcceptedInIntake(String projectId, XnatImagesessiondataI imageSession,
			List<SanityCheck> checkList, UserI user) {
		Map<String, List<SanityCheck>> comparisonMap = null; 
		for (final SanityCheck check : checkList) {
			if (check.getOutcome().equals(Outcome.FAILED)) {
				comparisonMap = initializeComparisonMap(projectId, imageSession, user);
				break;
			}
		}
		if (comparisonMap == null) {
			return;
		}
		outerLoop:
		for (final SanityCheck check : checkList) {
			if (check.getOutcome().equals(Outcome.FAILED)) {
				if (comparisonMap.containsKey(check.getScanNumber())) {
					final List<SanityCheck> compList = comparisonMap.get(check.getScanNumber());
					for (SanityCheck compCheck : compList) {
						if (check.getCheckShortName().equals(compCheck.getCheckShortName()) &&
								compCheck.getOutcome().equals(Outcome.ACCEPTED)) {
							check.setOutcome(Outcome.ACCEPTED);
							check.setAcceptJustification("Auto-accepted check per source-session sanity checks acceptance.");
							check.setAcceptUsername("System");
							continue outerLoop;
						}
					}
				}
			}
		}
		
	}

	private Map<String, List<SanityCheck>> initializeComparisonMap(String projectId,
			XnatImagesessiondataI imageSession, UserI user) {
		Map<String, List<SanityCheck>> comparisonMap = new HashMap<>();
		if (imageSession == null || !(imageSession instanceof XnatMrsessiondata)) {
			return null;
		}
		XnatMrsessiondata mrSession = (XnatMrsessiondata)imageSession;
		XnatProjectdata sourceProj = null;
		XnatSubjectdata sourceSubj = null;
		CcfReleaseRulesI releaseRules = null;
		List<XnatMrsessiondata> sourceSessions = null;
		for (final String SOURCE_PROJECT : Arrays.asList(SOURCE_PROJECTS)) {
			sourceProj = XnatProjectdata.getXnatProjectdatasById(SOURCE_PROJECT, user, false);
			if (sourceProj == null) {
				continue;
			}
			sourceSubj = XnatSubjectdata.GetSubjectByProjectIdentifier(sourceProj.getId(), mrSession.getSubjectData().getLabel(), user, false);
			if (sourceSubj == null) {
				continue;
			}
			releaseRules = _releaseRulesUtils.getReleaseRulesForProject(SOURCE_PROJECT).getReleaseRules();
			if (releaseRules == null) {
				continue;
			}
			final Map<String,String> paramMap = new HashMap<>();
			try {
				paramMap.put("selectionCriteria", getSelectionCriteriaFromSession(imageSession));
			} catch (Exception e) {
				log.error(ExceptionUtils.getStackTrace(e));
			}
			try {
				sourceSessions = CcfSessionBuildingUtils.getFilteredExperimentList(sourceProj, sourceSubj, releaseRules, paramMap, user);
			} catch (SessionBuildingException | ReleaseRulesException e) {
				continue;
			}
		}
		if (sourceSessions == null || sourceSessions.size()<1) {
			return new HashMap<>();
		}
		for (final XnatMrsessiondata sourceSession : sourceSessions) {
			final org.nrg.xft.search.CriteriaCollection cc = new CriteriaCollection("AND");
	      	cc.addClause("san:SanityChecks.imageSession_ID",sourceSession.getId());
	      	cc.addClause("san:SanityChecks.project",sourceSession.getProject());
	      	final List<SanSanitychecks> schecks = SanSanitychecks.getSanSanitycheckssByField(cc, user, false);
	      	for (final SanSanitychecks scheck : schecks) {
	      		
	    		for (final SanSanitychecksCheckI sessionCheck : scheck.getSession_sessionChecks_check()) {
	    			if (sessionCheck.getStatus().equalsIgnoreCase(Outcome.ACCEPTED.toString())) {
	    				addToMap(comparisonMap, "", sessionCheck, sourceSession);
	    			}
	    		}
	    		for (final SanSanitychecksScanChecksI scanCheck : scheck.getScans_scanChecks()) {
    				for (final SanSanitychecksScanChecksCheckI check : scanCheck.getCheck()) {
    					if (check.getStatus().equalsIgnoreCase(Outcome.ACCEPTED.toString())) {
    						final XnatImagescandataI scan = getScanForCheck(scanCheck, sourceSession);
    						if (scan == null) {
    							continue;
    						}
    						addToMap(comparisonMap, "", check, scan);
    					}
    				}
    			}
    		}
		}
		return comparisonMap;
	}
	

	protected void doSessionBuildingCheck(XnatImagesessiondataI imageSession, List<SanityCheck> checkList, UserI user) {
		
        final SanityCheck sCheck = new SanityCheck(imageSession);
        sCheck.setOutcome(Outcome.PASSED);
        sCheck.setCheckShortName(SESSION_BUILDING_CHECK_SHORT_NAME);
        sCheck.setCheckLongName(SESSION_BUILDING_CHECK_LONG_NAME);
        try {
			if (!compareCurrentWithRebuiltSession(imageSession,user)) {
				sCheck.setOutcome(Outcome.FAILED);
				sCheck.setDescriptionOfFailure("Scan list doesn't match rebuilt session");
				checkList.add(sCheck);
			}
		} catch (Exception e) {
			sCheck.setOutcome(Outcome.FAILED);
			sCheck.setDescriptionOfFailure("Exception thrown comparing session with rebuilt session");
			checkList.add(sCheck);
			log.error("Exception thrown comparing sessions", e);
		} 
       	checkList.add(sCheck);
		
	}

	protected boolean compareCurrentWithRebuiltSession(XnatImagesessiondataI imageSession, UserI user) throws Exception {
		
		if (imageSession == null || !(imageSession instanceof XnatMrsessiondata)) {
			throw new SessionBuildingException("Source session must be an MR Session");
		}
		XnatProjectdata sourceProj = null;
		XnatSubjectdata sourceSubj = null;
		CcfReleaseRulesI releaseRules = null;
		XnatMrsessiondata mrSession = (XnatMrsessiondata)imageSession;
		for (final String SOURCE_PROJECT : Arrays.asList(SOURCE_PROJECTS)) {
			sourceProj = XnatProjectdata.getXnatProjectdatasById(SOURCE_PROJECT, user, false);
			if (sourceProj == null) {
				continue;
			}
			sourceSubj = CcfReleaseRulesUtils.getSourceSubjectForProject(mrSession, sourceProj, user);
			if (sourceSubj == null) {
				continue;
			}
			releaseRules = _releaseRulesUtils.getReleaseRulesForProject(SOURCE_PROJECT).getReleaseRules();
			if (releaseRules == null) {
				continue;
			}
			final Map<String,String> paramMap = new HashMap<>();
			paramMap.put("errorOverride", "true");
			String selectionCriteria = getSelectionCriteriaFromSession(imageSession);
	
			if (selectionCriteria != null) {
				paramMap.put("selectionCriteria", selectionCriteria);
			}
			final ProcessingResults rebuildResults;
			try {
				rebuildResults = CcfSessionBuildingUtils.processSubject(sourceProj, sourceSubj, releaseRules, paramMap, user);
				if (rebuildResults == null) {
					continue;
				}
			} catch (Exception e) {
				continue;
			}
			final List<XnatMrsessiondata> rebuildSessionList = rebuildResults.getSessionList();
			if (rebuildSessionList == null || rebuildSessionList.size()<1) {
				continue;
			}
			final XnatMrsessiondata rebuildSession = CcfSessionBuildingUtils.buildAndReturnCombinedSession(sourceProj, sourceSubj, paramMap, releaseRules, rebuildSessionList, user);
			if (rebuildSession == null) {
				continue;
			}
			Map<String,String> rebuildMap = new TreeMap<>(getScanMap(rebuildSession));
			Map<String,String> currentMap = new TreeMap<>(getScanMap(mrSession));
			if (rebuildSession.getScans_scan().size() != mrSession.getScans_scan().size()) {
				log.error("Session rebuild check failed (SESSION=" + mrSession.getLabel() + "): \nSTAGING SESSION:  " +
						currentMap.toString() + "\nREBUILD SESSION: " + rebuildMap.toString());
				return false;
			};
			for (Entry<String, String> entry : rebuildMap.entrySet()) {
				if (!currentMap.containsKey(entry.getKey()) || !entry.getValue().equals(currentMap.get(entry.getKey()))) {
					log.error("Session rebuild check failed (SESSION=" + mrSession.getLabel() + "): \nSTAGING SESSION:  " +
							currentMap.toString() + "\nREBUILD SESSION: " + rebuildMap.toString());
					return false;
				} 
			}
			return true;
		}
		return false;
	}
	
	
private XnatImagescandataI getScanForCheck(SanSanitychecksScanChecksI scanCheck, XnatMrsessiondata sourceSession) {
	for (final XnatImagescandataI scan : sourceSession.getScans_scan()) {
		if (scan.getId().equals(scanCheck.getScanId())) {
			return scan;
		}
	}
	return null;
}

private void addToMap(Map<String, List<SanityCheck>> comparisonMap, String string, SanSanitychecksScanChecksCheckI check,
		XnatImagescandataI scan) {
	final SanityCheck newCheck = new SanityCheck(scan);
	newCheck.setCheckLongName(check.getDescription());
	newCheck.setCheckShortName(check.getId());
	newCheck.setDescriptionOfFailure(check.getDiagnosis());
	newCheck.setOutcome(Outcome.valueOf(check.getStatus()));
	final String dbId = scan.getDbid();
	if (!comparisonMap.containsKey(dbId)) {
		comparisonMap.put(dbId, new ArrayList<SanityCheck>());
	}
	comparisonMap.get(dbId).add(newCheck);
}

private void addToMap(Map<String, List<SanityCheck>> comparisonMap, String string, SanSanitychecksCheckI sessionCheck,
		XnatMrsessiondata sourceSession) {
	final SanityCheck newCheck = new SanityCheck(sourceSession);
	newCheck.setCheckLongName(sessionCheck.getDescription());
	newCheck.setCheckShortName(sessionCheck.getId());
	newCheck.setDescriptionOfFailure(sessionCheck.getDiagnosis());
	newCheck.setOutcome(Outcome.valueOf(sessionCheck.getStatus()));
	final String dbId = "";
	if (!comparisonMap.containsKey(dbId)) {
		comparisonMap.put(dbId, new ArrayList<SanityCheck>());
	}
	comparisonMap.get(dbId).add(newCheck);
}


protected Map<String, String> getScanMap(XnatMrsessiondata thisSession) {
	final Map<String, String> returnMap = new HashMap<>();
	for (XnatImagescandataI scan : thisSession.getScans_scan()) {
		returnMap.put(scan.getId(), scan.getSeriesDescription());
	}
	return returnMap;
	
}

}
