package org.nrg.ccf.sanitychecks.components.checkers.project;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.components.abst.AbstractCcfStagingSanityChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.ResourceVerificationChecker;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@SanityChecker(description = "Java-based sanity checks for CCF_BANDA_STG")
public class CcfBandaStagingChecker extends AbstractCcfStagingSanityChecker {
	
   	public static final String HAS_LINKEDDATA_CHECK_SHORT_NAME = "linked_data";
   	public static final String HAS_LINKEDDATA_CHECK_LONG_NAME = "Has Linked data resource?";
   	
   	public static final String TSVEV_CHECK_SHORT_NAME = "TSV_EV_CHECK";
   	public static final String TSVEV_CHECK_LONG_NAME = "TSV and EV files generated?";
   	
   	public static final String HAS_TASKCSV_CHECK_SHORT_NAME = "has_taskcsv";
   	public static final String HAS_TASKCSV_CHECK_LONG_NAME = "Has task csv file?";
   	
   	public static final String HAS_RESTCSV_CHECK_SHORT_NAME = "has_restcsv";
   	public static final String HAS_RESTCSV_CHECK_LONG_NAME = "Has rest csv file?";

	public static final String 
			TFMRI_RGX = "(?i)^tfMRI.*", 
			RFMRI_RGX = "(?i)^rfMRI.*", 
			DMRI_RGX = "(?i)^dMRI.*",
			SBREF_RGX = "(?i)^.*_SBRef$", 
			GAMBLING_RGX = "(?i)^.*GAMBLING.*$", 
			FACENAME_RGX = "(?i)^.*FACENAME.*$", 
			CONFLICT1_RGX = "(?i)^.*CONFLICT1.*$", 
			CONFLICT2_RGX = "(?i)^.*CONFLICT2.*$", 
			TASKCSV_RGX = "(?i)^.*_run-.*csv$", 
			RESTCSV_RGX = "(?i)^.*_Resting_Run.*csv$" 
			;
	
	protected List<String> MAIN_SCAN_TYPES = Arrays.asList(new String[] {"rfMRI","tfMRI","dMRI"});
	protected List<String> LINKED_DATA_REGEXES = Arrays.asList(new String[] {"^rfMRI$","^tfMRI$"});

	@Lazy
	@Autowired
	public CcfBandaStagingChecker(ResourceVerificationChecker resourceVerificationChecker, CcfReleaseRulesUtils releaseRulesUtils) {
		super(resourceVerificationChecker, releaseRulesUtils);
	}

	@Override
	public void initializeSourceProjectValue() {
		SOURCE_PROJECTS = new String[] {"CCF_BANDA_PRC"};
	}

	@Override
	public void doNonStagingNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user,
			List<SanityCheck> checkList) throws SanityChecksException {
		log.debug("Run CCF BANDA Staging Sanity Checks");
		// No other project specific checks for BANDA
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			doScanLevelLinkedDataChecks(scan, checkList);
			doTsvEvCheck(scan, checkList);
		}
	}
	
	@Override
	public String getSelectionCriteriaFromSession(XnatImagesessiondataI session) {
		return "ALL";
	}

	@Override
	public void initializeScanTypeNiftiFileCountMap() {
        _scanTypeNiftiFileCountMap.put("AAHScout", new Integer[] {2, 6, 8});
        _scanTypeNiftiFileCountMap.put("FieldMap_SE_EPI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("Bias_Receive", new Integer[] {2, 6, 8});
        _scanTypeNiftiFileCountMap.put("Localizer", new Integer[] {6, 8});
        _scanTypeNiftiFileCountMap.put("Localizer_aligned", new Integer[] {6, 8});
        _scanTypeNiftiFileCountMap.put("T1w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("rfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("rfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("tfMRI", new Integer[] {2, 3});
        _scanTypeNiftiFileCountMap.put("tfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("dMRI", new Integer[] {4});
	// 2023/01/02:  dMRI SBRefs now sometimes have bvec/bval files.  Okaying "4" as a valid count, in addition to "2"
	// Per MH, it's okay for SBRefs to have bvec/bval files.
        _scanTypeNiftiFileCountMap.put("dMRI_SBRef", new Integer[] {2,4});
	}

	@Override
	public void initializeSeriesDescNiftiFileCountMap() {
        _seriesDescNiftiFileCountMap.put("tfMRI_GAMBLING_AP", new Integer[] {3});
        _seriesDescNiftiFileCountMap.put("tfMRI_GAMBLING_PA", new Integer[] {3});
        _seriesDescNiftiFileCountMap.put("tfMRI_FACEMATCHING_AP", new Integer[] {2});
        _seriesDescNiftiFileCountMap.put("tfMRI_FACEMATCHING_PA", new Integer[] {2});
        _seriesDescNiftiFileCountMap.put("tfMRI_CONFLICT1_AP", new Integer[] {3});
        _seriesDescNiftiFileCountMap.put("tfMRI_CONFLICT1_PA", new Integer[] {3});
        _seriesDescNiftiFileCountMap.put("tfMRI_CONFLICT2_AP", new Integer[] {3});
        _seriesDescNiftiFileCountMap.put("tfMRI_CONFLICT2_PA", new Integer[] {3});
	}
	

	protected void doScanLevelLinkedDataChecks(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		final String scanQuality = scan.getQuality();
        final String currScanType = scan.getType();
        boolean isLinkedDataScan = false;
        for (final String regex : LINKED_DATA_REGEXES) {
        	if (currScanType.matches(regex)) {
        		isLinkedDataScan = true;
        	}
		}
		if ((!isLinkedDataScan) || currScanType.matches(SBREF_RGX) || scanQuality.equalsIgnoreCase("UNUSABLE")) {
			return;
		}
        final SanityCheck sCheck = new SanityCheck(scan);
        sCheck.setOutcome(Outcome.PASSED);
        sCheck.setCheckShortName(HAS_LINKEDDATA_CHECK_SHORT_NAME);
        sCheck.setCheckLongName(HAS_LINKEDDATA_CHECK_LONG_NAME);
        final XnatResourcecatalog linkedDataResource = ResourceUtils.getResource(scan, CommonConstants.LINKED_DATA_RESOURCE);
        if (linkedDataResource == null) {
        	sCheck.setOutcome(Outcome.FAILED);
        	sCheck.setDescriptionOfFailure("Linked data not uploaded");
        	checkList.add(sCheck);
        	// To be consistent with earlier automation-based checks, let's just skip individual file checks
        	// when we don't have a linked data resource
        	return;
        } else { 
        	checkList.add(sCheck);
        }
        
		if (currScanType.matches(RFMRI_RGX)) {
			
			final SanityCheck restCsvCheck = new SanityCheck(scan);
			restCsvCheck.setOutcome(Outcome.PASSED);
			restCsvCheck.setCheckShortName(HAS_RESTCSV_CHECK_SHORT_NAME);
			restCsvCheck.setCheckLongName(HAS_RESTCSV_CHECK_LONG_NAME);
			if (!hasMatchingFile(linkedDataResource,RESTCSV_RGX )) {
				restCsvCheck.setOutcome(Outcome.FAILED);
				restCsvCheck.setDescriptionOfFailure("Resting state csv file does not exist in resource.");
			}
			checkList.add(restCsvCheck);
			
		}
		else if (currScanType.matches(TFMRI_RGX)) {
			
			final SanityCheck designCheck = new SanityCheck(scan);
			designCheck.setOutcome(Outcome.PASSED);
			designCheck.setCheckShortName(HAS_TASKCSV_CHECK_SHORT_NAME);
			designCheck.setCheckLongName(HAS_TASKCSV_CHECK_LONG_NAME);
			if (!hasMatchingFile(linkedDataResource,TASKCSV_RGX)) {
				designCheck.setOutcome(Outcome.FAILED);
				designCheck.setDescriptionOfFailure("Task csv does not exist in resource.");
			}
			checkList.add(designCheck);
			
		}
        
    } 
	
	protected void doTsvEvCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		final String scanType = scan.getType();
		final String scanQuality = scan.getQuality();
        final String currSeriesDesc = scan.getSeriesDescription();
       	if (!scanType.matches(TFMRI_RGX) || scanType.matches(SBREF_RGX)) {
			return;
       	}
		if (scanQuality.equalsIgnoreCase("UNUSABLE")) {
			return;
		}
        SanityCheck sCheck = new SanityCheck(scan);
        sCheck.setOutcome(Outcome.PASSED);
        sCheck.setCheckLongName(TSVEV_CHECK_LONG_NAME);
        sCheck.setCheckShortName(TSVEV_CHECK_SHORT_NAME);
        XnatResourcecatalog linkedDataResource = null;
        final StringBuilder probSB = new StringBuilder();
        if (!hasTsv(scan,linkedDataResource)) {
        	probSB.append("Missing generated TSV file");
        	sCheck.setOutcome(Outcome.FAILED);
        }
        if (currSeriesDesc.matches(TFMRI_RGX)) { 
        	if (!hasEv(scan,linkedDataResource)) {
        		if (!probSB.toString().isEmpty()) {
        			probSB.append(",");
        		}
        		probSB.append("Missing generated EV file(s)");
        		sCheck.setOutcome(Outcome.FAILED);
        	}
        }
        if (sCheck.getOutcome().equals(Outcome.FAILED)) {
        	sCheck.setDescriptionOfFailure(probSB.toString());
        }
       	checkList.add(sCheck);
    } 
	
	private boolean hasMatchingFile(XnatResourcecatalog linkedDataResource, String rgx) {
		if (linkedDataResource == null) {
			return false;
		}
		List<String> fileNames = ResourceUtils.getFileNames(linkedDataResource);
		for (final String fName : fileNames) {
			if (fName.matches(rgx)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean hasEv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("/EVs/")) {
				return true;
			}
		}
		return false;
	}
	
	private boolean hasTsv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("/TSV/")) {
				return true;
			}
		}
		return false;
	}
	
	private XnatResourcecatalog initializeLinkedDataResource(final XnatImagescandataI scan) {
		for (final XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			if (resource.getLabel().equals(CommonConstants.LINKED_DATA_RESOURCE)) {
				return (XnatResourcecatalog)resource;
			}
		}
		return null;
	}

}
	
