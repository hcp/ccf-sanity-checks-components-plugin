package org.nrg.ccf.sanitychecks.components.checkers.project;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sanitychecks.abst.AbstractSanityChecker;
import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.ValidationFailureChecker;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@Primary
@SanityChecker(description = "Java-based sanity checks for CCF_HCA_ITK")
public class CcfHcaChecker extends AbstractSanityChecker {
	
   	public static final String COMPLETION_CHECK_SHORT_NAME = "COMPLETION_CHECK";
   	public static final String COMPLETION_CHECK_LONG_NAME = "Enough frames to release?";
   	public static final String PHYSIOEV_CHECK_SHORT_NAME = "PHYSIO_EV_CHECK";
   	public static final String PHYSIOEV_CHECK_LONG_NAME = "Physio/EV files generated?";
   	
	public static final String 
			TFMRI_RGX = "(?i)^tfMRI.*", 
			RFMRI_RGX = "(?i)^rfMRI.*", 
			DMRI_RGX = "(?i)^dMRI.*",
			SBREF_RGX = "(?i)^.*_SBRef$" 
			;
	
	private List<String> MAIN_SCAN_TYPES = Arrays.asList(new String[] {"rfMRI","tfMRI","dMRI"});
	
	protected final ValidationFailureChecker _validationFailureChecker;
	
	@Autowired
	public CcfHcaChecker(ValidationFailureChecker _validationFailureChecker) {
		super();
		this._validationFailureChecker = _validationFailureChecker;
		this.addSessionChecksToRemove(Arrays.asList(new String[] {"protocolVal"}));
		this.addScanChecksToRemove(Arrays.asList(new String[] {"has_facename","has_eyecam"}));
	}

	@Override
	public List<SanityCheck> doChecks(String projectId, XnatImagesessiondataI imageSession, UserI user) throws SanityChecksException {
		
		log.debug("Run CCF HCA Sanity Checks");
		
		final List<SanityCheck> checkList = new ArrayList<>();
		
		checkList.addAll(_validationFailureChecker.doChecks(projectId, imageSession, user));
		
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			doSufficientFramesCheck(scan, checkList);
			doPhysioEvCheck(scan, checkList);
			
		}
		
		return checkList;
	}

	protected void doPhysioEvCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		final String scanType = scan.getType();
		final String scanQuality = scan.getQuality();
        final String currSeriesDesc = scan.getSeriesDescription();
		if (!MAIN_SCAN_TYPES.contains(scanType)) {
			return;
		}
		if (scanQuality.equalsIgnoreCase("UNUSABLE")) {
			return;
		}
        SanityCheck sCheck = new SanityCheck(scan);
        sCheck.setOutcome(Outcome.PASSED);
        sCheck.setCheckLongName(PHYSIOEV_CHECK_LONG_NAME);
        sCheck.setCheckShortName(PHYSIOEV_CHECK_SHORT_NAME);
        XnatResourcecatalog linkedDataResource = null;
        final StringBuilder probSB = new StringBuilder();
        if (!hasPhysioCsv(scan,linkedDataResource) && !scan.getType().equals("dMRI")) {
        	probSB.append("Missing Physio TXT file");
        	sCheck.setOutcome(Outcome.FAILED);
        }
        if (currSeriesDesc.matches(TFMRI_RGX)) { 
        	if (!hasWideCsvAndEv(scan,linkedDataResource)) {
        		if (!probSB.toString().isEmpty()) {
        			probSB.append(",");
        		}
        		probSB.append("Missing generated wide CSV and/or EV file(s)");
        		sCheck.setOutcome(Outcome.FAILED);
        	}
        }
        if (sCheck.getOutcome().equals(Outcome.FAILED)) {
        	sCheck.setDescriptionOfFailure(probSB.toString());
        }
       	checkList.add(sCheck);
    } 

	protected void doSufficientFramesCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		log.debug("Running frames check (SCAN=" + scan.getId() + ")");
        final String currSeriesDesc = scan.getSeriesDescription();
       	if (currSeriesDesc == null || currSeriesDesc.matches(SBREF_RGX) || currSeriesDesc.toUpperCase().contains("PHYSIO")) {
                return;
        }
        if (currSeriesDesc.matches(RFMRI_RGX) || currSeriesDesc.matches(TFMRI_RGX)) { 
        	
        	SanityCheck sCheck = new SanityCheck(scan);
        	sCheck.setCheckLongName(COMPLETION_CHECK_LONG_NAME);
        	sCheck.setCheckShortName(COMPLETION_CHECK_SHORT_NAME);
            if (scan.getFrames() == null) {
            	sCheck.setOutcome(Outcome.FAILED);
            	sCheck.setDescriptionOfFailure("Could not determine frame count");
            }
            int frames = scan.getFrames().intValue();
            int frames10 = frames - 10;
            //String scanComplete = null;
            String scan100 = null;
            double scanPct = 0;
            if (currSeriesDesc.matches(RFMRI_RGX)) { 
            	//scanComplete = (frames>=488) ? "Y" : "N";
           		scanPct = (double)frames10/(488-10);
            	scan100 = (frames10>=100) ? "Y" : "N";
            	if (scan100.equals("N")) {
            		sCheck.setOutcome(Outcome.FAILED);
            		sCheck.setDescriptionOfFailure("Insufficient frames (" + frames + ")");
            	} else {
            		sCheck.setOutcome(Outcome.PASSED);
            	}
            } else if (currSeriesDesc.matches(TFMRI_RGX)) { 
            	if (currSeriesDesc.contains("_VISMOTOR")) {
            		//scanComplete = (frames>=194) ? "Y" : "N";
            		scanPct = (double)frames10/(194-10);
            	} else if (currSeriesDesc.contains("_CARIT")) {
            		//scanComplete = (frames>=300) ? "Y" : "N";
            		scanPct = (double)frames10/(300-10);
            	} else if (currSeriesDesc.contains("_FACENAME")) {
            		//scanComplete = (frames>=345) ? "Y" : "N";
            		scanPct = (double)frames10/(345-10);
            	} 
            	if (scanPct<.75) {
            		sCheck.setOutcome(Outcome.FAILED);
            		sCheck.setDescriptionOfFailure("Insufficient frames (" + frames + ")");
            	} else {
            		sCheck.setOutcome(Outcome.PASSED);
            	}
            } 
            if (sCheck.getOutcome() != null) {
            	checkList.add(sCheck);
            }
        } 
        
	}
		
	private boolean hasWideCsvAndEv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		boolean hasWideCsv = false;
		boolean hasGeneratedWide = false;
		boolean hasEV = false;
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fn = f.getName().toLowerCase();
			if (fn.contains("_wide.csv")) {
				hasWideCsv = true;
				if (fn.matches("^.*_run[0-9]_wide.csv")) {
					hasGeneratedWide = true;
				}
			} else {
				final String fp = f.getAbsolutePath();
				if (fp.contains("/EVs/")) {
					hasEV = true;
				}
			}
			if (hasWideCsv && hasGeneratedWide && hasEV) {
				return true;
			}
		}
		return false;
	}

	private boolean hasPhysioCsv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (!fp.contains("/PHYSIO/")) {
				continue;
			}
			if (fp.toLowerCase().endsWith(".csv")) {
				return true;
			}
		}
		return false;
	}

	private XnatResourcecatalog initializeLinkedDataResource(final XnatImagescandataI scan) {
		for (final XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			if (resource.getLabel().equals(CommonConstants.LINKED_DATA_RESOURCE)) {
				return (XnatResourcecatalog)resource;
			}
		}
		return null;
	}

}
