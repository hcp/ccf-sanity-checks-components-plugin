package org.nrg.ccf.sanitychecks.components.checkers.project;

import java.util.List;

import org.nrg.ccf.sanitychecks.abst.AbstractNiftiCheckerSanityChecker;
import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

//import lombok.extern.slf4j.Slf4j;

/**
 * The Class CcfBandaChecker to execute sanity checks for CCF_BANDA_PRC project.
 * @author Atul
 */

//@Slf4j
@Lazy
@Component
@Primary
@SanityChecker(description = "Java-based sanity checks for CCF_BANDA_PRC")
public class CcfBwhChecker extends AbstractNiftiCheckerSanityChecker {

	/** The Constant SBREF_RGX. */
	public static final String 
	RFMRI_RGX = "(?i)^.*fMRI_r.*", 
	DMRI_RGX = "(?i)^.*dMRI.*",
	SBREF_RGX = "(?i)^.*_SBRef$" 
	;

	/** The main scan types. */
	private JdbcTemplate _jdbcTemplate;

	/**
	 * Instantiates a new ccf BANDA checker.
	 */
	@Autowired
	@Lazy
	public CcfBwhChecker(JdbcTemplate jdbcTemplate) {
		super();
		_jdbcTemplate = jdbcTemplate;
		this._seriesDescFrameCountExclusionRegexes.add("(?i)^resolve.*");
	}

	@Override
	public void initializeScanTypeNiftiFileCountMap() {
        _scanTypeNiftiFileCountMap.put("AAHScout", new Integer[] {2, 6, 8});
        _scanTypeNiftiFileCountMap.put("FieldMap_SE_EPI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("Bias_Receive", new Integer[] {2, 6, 8});
        _scanTypeNiftiFileCountMap.put("Localizer", new Integer[] {6, 8});
        _scanTypeNiftiFileCountMap.put("Localizer_aligned", new Integer[] {6, 8});
        _scanTypeNiftiFileCountMap.put("T1w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("rfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("rfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("tfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("tfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("dMRI", new Integer[] {4});
	// 2023/01/02:  dMRI SBRefs now sometimes have bvec/bval files.  Okaying "4" as a valid count, in addition to "2"
	// Per MH, it's okay for SBRefs to have bvec/bval files.
        _scanTypeNiftiFileCountMap.put("dMRI_SBRef", new Integer[] {2,4});
	}
	
	@Override
	protected void initializeInitialFramesMaps(String projectId) {
		super.initializeInitialFramesMaps(projectId);
		if (_initialFramesDescMap.get(projectId).isEmpty() && projectId.toUpperCase().contains("BWH")) {
			final List<String> bandaList = _jdbcTemplate.queryForList(
					"SELECT DISTINCT b.series_description FROM xnat_experimentdata a, xnat_imagescandata b " +
					  "WHERE a.project='CCF_BWH_PRC' AND a.id=b.image_session_id AND " + 
					      "LOWER(b.series_description) SIMILAR TO '%(_rest)%' AND " +
					      "LOWER(b.series_description) NOT LIKE '%_sbref';"
					, String.class); 
			_initialFramesDescMap.get(projectId).addAll(bandaList);
		}
	}

	
	/* (non-Javadoc)
	 * @see org.nrg.ccf.sanitychecks.inter.SanityCheckerI#doChecks(java.lang.String, org.nrg.xdat.model.XnatImagesessiondataI, org.nrg.xft.security.UserI)
	 */
	@Override
	public void doNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user, List<SanityCheck> checkList) {
		
		// No non-NIFTI checks for BWN (No linked data or physio files)

	}
	
}
