package org.nrg.ccf.sanitychecks.components.checkers.project;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sanitychecks.abst.AbstractNiftiCheckerSanityChecker;
import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class CcfMDDChecker to execute sanity checks for CCF_MDD_PRC project.
 * @author Atul
 */

@Slf4j
@Component
@Primary
@SanityChecker(description = "Java-based sanity checks for CCF_MDD_PRC")
public class CcfMDDChecker extends AbstractNiftiCheckerSanityChecker {

	/** The Constant COMPLETION_CHECK_SHORT_NAME. */
	public static final String COMPLETION_CHECK_SHORT_NAME = "COMPLETION_CHECK";

	/** The Constant COMPLETION_CHECK_LONG_NAME. */
	public static final String COMPLETION_CHECK_LONG_NAME = "Enough frames to release?";

	/** The Constant PHYSIO_CHECK_SHORT_NAME. */
	public static final String PHYSIO_CHECK_SHORT_NAME = "PHYSIO_CHECK";
	
	/** The Constant PHYSIO_CHECK_LONG_NAME. */
	public static final String PHYSIO_CHECK_LONG_NAME = "Physio files generated?";

	/** The Constant EV_CHECK_SHORT_NAME. */
	public static final String EV_CHECK_SHORT_NAME = "EV_CHECK";
	
	/** The Constant EV_CHECK_LONG_NAME. */
	public static final String EV_CHECK_LONG_NAME = "EV files generated/uploaded (PSYCHOPY/EVs folder)?";

	/** The Constant EVMULT_CHECK_SHORT_NAME. */
	public static final String EVMULT_CHECK_SHORT_NAME = "EVMULT_CHECK";

	/** The Constant EVMULT_CHECK_LONG_NAME. */
	public static final String EVMULT_CHECK_LONG_NAME = "Should not have both /EVs and PSYCHOPY/EVs folders (Else need script rerun)";
	
	/** The Constant HAS_CARIT_CSV_SHORT_NAME. */
	public static final String HAS_CARIT_CSV_SHORT_NAME = "CARIT_CSV_CHECK";
	
	/** The Constant HAS_CARIT_CSV_LONG_NAME. */
	public static final String HAS_CARIT_CSV_LONG_NAME = "Has session level CARIT csv(s) (if carit scans)";
	
	/** The Constant HAS_FACEMATCHING_CSV_SHORT_NAME. */
	public static final String HAS_FACEMATCHING_CSV_SHORT_NAME = "FACEMATCHING_CSV_CHECK";
	
	/** The Constant HAS_FACEMATCHING_CSV_LONG_NAME. */
	public static final String HAS_FACEMATCHING_CSV_LONG_NAME = "Has session level FACEMATCHING csv (if facename scans)";

	///** The Constant PHYSIOEV_CHECK_SHORT_NAME. */
	//public static final String PHYSIOEV_CHECK_SHORT_NAME = "PHYSIO_EV_CHECK";

	///** The Constant PHYSIOEV_CHECK_LONG_NAME. */
	//public static final String PHYSIOEV_CHECK_LONG_NAME = "Physio/EV files generated?";

	/** The Constant SBREF_RGX. */
	public static final String 
	TFMRI_RGX = "(?i)^tfMRI.*", 
	RFMRI_RGX = "(?i)^rfMRI.*", 
	DMRI_RGX = "(?i)^dMRI.*",
	SBREF_RGX = "(?i)^.*_SBRef$" 
	;

	/** The main scan types. */
	// Currently no checks for diffusion (not generating diffusion physio)
	private List<String> MAIN_SCAN_TYPES = Arrays.asList("rfMRI","tfMRI");
	//private List<String> MAIN_SCAN_TYPES = Arrays.asList("rfMRI","tfMRI","dMRI");
	private JdbcTemplate _jdbcTemplate;

	/**
	 * Instantiates a new ccf MDD checker.
	 */
	@Autowired
	@Lazy
	public CcfMDDChecker(JdbcTemplate jdbcTemplate) {
		super();
		this.addScanChecksToRemove(Arrays.asList("PHYSIO_EV_CHECK","EV_ORIG_CHECK","EVORIG_CHECK"));
		_jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public void initializeScanTypeNiftiFileCountMap() {
        _scanTypeNiftiFileCountMap.put("AAHScout", new Integer[] {2, 6, 8});
        _scanTypeNiftiFileCountMap.put("FieldMap_SE_EPI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("Bias_Receive", new Integer[] {2, 6, 8});
        _scanTypeNiftiFileCountMap.put("Localizer", new Integer[] {6});
        _scanTypeNiftiFileCountMap.put("Localizer_aligned", new Integer[] {6});
        _scanTypeNiftiFileCountMap.put("Cal_800TR", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("Cal_800TR_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_4e", new Integer[] {8});
        _scanTypeNiftiFileCountMap.put("T1w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_Norm_4e", new Integer[] {8});
        _scanTypeNiftiFileCountMap.put("T1w_setter", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_setter", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("rfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("rfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("tfMRI", new Integer[] {2,3});
        _scanTypeNiftiFileCountMap.put("tfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("dMRI", new Integer[] {4});
	// 2023/01/02:  dMRI SBRefs now sometimes have bvec/bval files.  Okaying "4" as a valid count, in addition to "2"
	// Per MH, it's okay for SBRefs to have bvec/bval files.
        _scanTypeNiftiFileCountMap.put("dMRI_SBRef", new Integer[] {2,4});
        _scanTypeNiftiFileCountMap.put("mbPCASLhr", new Integer[] {2});
	}
	

	@Override
	public void initializeSeriesDescNiftiFileCountMap() {
        _seriesDescNiftiFileCountMap.put("tfMRI_CARIT_AP", new Integer[] {3});
        _seriesDescNiftiFileCountMap.put("tfMRI_CARIT_PA", new Integer[] {3});
        _seriesDescNiftiFileCountMap.put("tfMRI_FACEMATCHING_AP", new Integer[] {2});
        _seriesDescNiftiFileCountMap.put("tfMRI_FACEMATCHING_PA", new Integer[] {2});
	}
	
	@Override
	protected void initializeInitialFramesMaps(String projectId) {
		super.initializeInitialFramesMaps(projectId);
		if (_initialFramesDescMap.get(projectId).isEmpty() && projectId.toUpperCase().contains("_MDD")) {
			final List<String> bandaList = _jdbcTemplate.queryForList(
					"SELECT DISTINCT b.series_description FROM xnat_experimentdata a, xnat_imagescandata b " +
					  "WHERE a.project='CCF_MDD_PRC' AND a.id=b.image_session_id AND " + 
					      "LOWER(b.series_description) SIMILAR TO '%(_carit|_rest)%' AND " +
					      "LOWER(b.series_description) NOT LIKE '%_sbref';"
					, String.class); 
			_initialFramesDescMap.get(projectId).addAll(bandaList);
		}
	}

	/* (non-Javadoc)
	 * @see org.nrg.ccf.sanitychecks.inter.SanityCheckerI#doChecks(java.lang.String, org.nrg.xdat.model.XnatImagesessiondataI, org.nrg.xft.security.UserI)
	 */
	/*
	@Override
	public List<SanityCheck> doChecks(String projectId, XnatImagesessiondataI imageSession, UserI user) {

	}
	*/

	@Override
	public void doNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user, List<SanityCheck> checkList) {

		log.debug("Run CCF MDD Sanity Checks");
		
		doSessionTfmriLinkedCsvChecks(imageSession, checkList);
		
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			final String scanType = scan.getType();
			if ((scanType != null && !MAIN_SCAN_TYPES.contains(scanType))) {
				continue;
			}
			doPhysioCheck(scan, checkList);
			doEvCheck(scan, checkList);
			doEvMultCheck(scan, checkList);
		}
		
	}

	/**
	 * Do session tfmri linked csv checks.
	 *
	 * @param imageSession the image session
	 * @param checkList the check list
	 */
	private void doSessionTfmriLinkedCsvChecks(XnatImagesessiondataI imageSession, List<SanityCheck> checkList) {
		
		boolean hasCaritScan = false;
		boolean hasFacematchingScan = false;
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			if (scan.getType() == null || scan.getSeriesDescription() == null) {
				continue;
			}
			if (scan.getType().equals("tfMRI")) {
				final String scanQuality = scan.getQuality();
				if (scanQuality != null && scanQuality.equalsIgnoreCase("UNUSABLE")) {
					continue;
				}
				if (scan.getSeriesDescription().toUpperCase().contains("CARIT")) {
					hasCaritScan = true;
				} else if (scan.getSeriesDescription().toUpperCase().contains("FACEMATCHING")) {
					hasFacematchingScan = true;
				}
			}
			if (hasCaritScan && hasFacematchingScan) {
				break;
			}
		}
		
		final SanityCheck caritCsvCheck = new SanityCheck(imageSession);
		caritCsvCheck.setCheckLongName(HAS_CARIT_CSV_LONG_NAME);
		caritCsvCheck.setCheckShortName(HAS_CARIT_CSV_SHORT_NAME);
		checkList.add(caritCsvCheck);
		
		final SanityCheck facematchingCsvCheck = new SanityCheck(imageSession);
		facematchingCsvCheck.setCheckLongName(HAS_FACEMATCHING_CSV_LONG_NAME);
		facematchingCsvCheck.setCheckShortName(HAS_FACEMATCHING_CSV_SHORT_NAME);
		checkList.add(facematchingCsvCheck);
		
		if (!hasCaritScan && !hasFacematchingScan) {
			caritCsvCheck.setOutcome(Outcome.PASSED);
			facematchingCsvCheck.setOutcome(Outcome.PASSED);
			return;
		}
		
		boolean hasFacematchingCsv = false;
		boolean hasCaritCsv = false;
		for (final XnatAbstractresourceI resource : imageSession.getResources_resource()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			if (resource.getLabel().equals(CommonConstants.LINKED_DATA_RESOURCE)) {
				final XnatResourcecatalog linkedDataResource = (XnatResourcecatalog)resource;
				final List<ResourceFile> files = linkedDataResource.getFileResources(CommonConstants.ROOT_PATH);
				for (final ResourceFile file : files) {
					final String fp = file.getAbsolutePath();
					if (fp.contains("/PSYCHOPY/") && fp.toLowerCase().contains(".csv")) {
						if (!hasFacematchingCsv && fp.toUpperCase().contains("FACEMATCHING")) {
							hasFacematchingCsv = true;
						} else if (fp.toUpperCase().contains("CARIT")) {
							hasCaritCsv = true;
						}
						if (hasFacematchingCsv && hasCaritCsv) {
							break;
						}
					}
				}
				break;
			}
		}
		
		if (hasCaritScan && !hasCaritCsv) {
			caritCsvCheck.setOutcome(Outcome.FAILED);
			caritCsvCheck.setDescriptionOfFailure("Session has CARIT scans but no session-level CARIT csv file.");
		} else {
			caritCsvCheck.setOutcome(Outcome.PASSED);
		}
		if (hasFacematchingScan && !hasFacematchingCsv) {
			facematchingCsvCheck.setOutcome(Outcome.FAILED);
			facematchingCsvCheck.setDescriptionOfFailure("Session has FACEMATCHING scans but no session-level FACEMATCHING csv file.");
		} else {
			facematchingCsvCheck.setOutcome(Outcome.PASSED);
		}
		
	}

	/**
	 * Do physio ev check.
	 *
	 * @param scan the scan
	 * @param checkList the check list
	 */
	protected void doPhysioCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {

		final SanityCheck sCheck = new SanityCheck(scan);
		sCheck.setOutcome(Outcome.PASSED);
		sCheck.setCheckLongName(PHYSIO_CHECK_LONG_NAME);
		sCheck.setCheckShortName(PHYSIO_CHECK_SHORT_NAME);
		XnatResourcecatalog linkedDataResource = null;
		final StringBuilder probSB = new StringBuilder();
		final String scanQuality = scan.getQuality();
		if (!(scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE"))) {
			if (!hasPhysio(scan,linkedDataResource)) {
				if (!probSB.toString().isEmpty()) {
					probSB.append(",");
				}
				probSB.append("Missing generated physio file(s)");
				sCheck.setOutcome(Outcome.FAILED);
			}
		} else {
			sCheck.setOutcome(Outcome.SCAN_UNUSABLE);
		}
		if (sCheck.getOutcome().equals(Outcome.FAILED)) {
			sCheck.setDescriptionOfFailure(probSB.toString());
		}
		checkList.add(sCheck);
	} 
	
	/**
	 * Do ev check.
	 *
	 * @param scan the scan
	 * @param checkList the check list
	 */
	protected void doEvCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {

		final String currSeriesDesc = scan.getSeriesDescription();
		if (currSeriesDesc == null || !currSeriesDesc.matches(TFMRI_RGX)) { 
			return;
		}
		final SanityCheck sCheck = new SanityCheck(scan);
		sCheck.setOutcome(Outcome.PASSED);
		sCheck.setCheckLongName(EV_CHECK_LONG_NAME);
		sCheck.setCheckShortName(EV_CHECK_SHORT_NAME);
		XnatResourcecatalog linkedDataResource = null;
		final StringBuilder probSB = new StringBuilder();
		final String scanQuality = scan.getQuality();
		if (!(scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE"))) {
			if (!hasEv(scan,linkedDataResource)) {
				if (!probSB.toString().isEmpty()) {
					probSB.append(",");
				}
				probSB.append("Missing EV file(s)");
				sCheck.setOutcome(Outcome.FAILED);
			}
		} else {
			sCheck.setOutcome(Outcome.SCAN_UNUSABLE);
		}
		if (sCheck.getOutcome().equals(Outcome.FAILED)) {
			sCheck.setDescriptionOfFailure(probSB.toString());
		}
		checkList.add(sCheck);
	} 

	/**
	 * Do ev_mult check.
	 *
	 * @param scan the scan
	 * @param checkList the check list
	 */
	protected void doEvMultCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {

		final String currSeriesDesc = scan.getSeriesDescription();
		if (currSeriesDesc == null || !currSeriesDesc.matches(TFMRI_RGX)) { 
			return;
		}
		final SanityCheck sCheck = new SanityCheck(scan);
		sCheck.setOutcome(Outcome.PASSED);
		sCheck.setCheckLongName(EVMULT_CHECK_LONG_NAME);
		sCheck.setCheckShortName(EVMULT_CHECK_SHORT_NAME);
		XnatResourcecatalog linkedDataResource = null;
		final StringBuilder probSB = new StringBuilder();
		final String scanQuality = scan.getQuality();
		if (!(scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE"))) {
			if (hasEv(scan,linkedDataResource) && hasNonpsychopyEv(scan,linkedDataResource)) {
				if (probSB.toString().isEmpty()) {
					probSB.append(",");
				}
				probSB.append("Has both /EVs and /PSYCHOPY/EVs files");
				sCheck.setOutcome(Outcome.FAILED);
			}
		} else {
			sCheck.setOutcome(Outcome.SCAN_UNUSABLE);
		}
		if (sCheck.getOutcome().equals(Outcome.FAILED)) {
			sCheck.setDescriptionOfFailure(probSB.toString());
		}
		checkList.add(sCheck);
	} 

	/**
	 * Checks for wide csv and ev.
	 *
	 * @param scan the scan
	 * @param linkedDataResource the linked data resource
	 * @return true, if successful
	 */
	private boolean hasPhysio(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}

		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("/PHYSIO/") && fp.toLowerCase().endsWith(".csv")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks for wide csv and ev.
	 *
	 * @param scan the scan
	 * @param linkedDataResource the linked data resource
	 * @return true, if successful
	 */
	private boolean hasEv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}

		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("PSYCHOPY/EVs/") && fp.contains(".txt")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks for original EV files.
	 *
	 * @param scan the scan
	 * @param linkedDataResource the linked data resource
	 * @return true, if successful
	 */
	private boolean hasNonpsychopyEv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("EVs/") && fp.contains(".txt") && !fp.contains("PSYCHOPY/")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Initialize linked data resource.
	 *
	 * @param scan the scan
	 * @return the xnat resourcecatalog
	 */
	private XnatResourcecatalog initializeLinkedDataResource(final XnatImagescandataI scan) {
		for (final XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			if (resource.getLabel().equals(CommonConstants.LINKED_DATA_RESOURCE)) {
				return (XnatResourcecatalog)resource;
			}
		}
		return null;
	}

}
