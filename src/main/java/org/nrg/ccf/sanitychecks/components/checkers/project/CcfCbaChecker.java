package org.nrg.ccf.sanitychecks.components.checkers.project;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sanitychecks.abst.AbstractNiftiCheckerSanityChecker;
import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;

/**
 * The Class CcfBandaChecker to execute sanity checks for CCF_CBA_PRC project.
 * @author Atul
 */

@Slf4j
@Lazy
@Component
@Primary
@SanityChecker(description = "Java-based sanity checks for CCF_CBA_PRC")
public class CcfCbaChecker extends AbstractNiftiCheckerSanityChecker {

	/** The Constant SBREF_RGX. */
	public static final String 
	TFMRI_RGX = "(?i)^tfMRI.*",
	RFMRI_RGX = "(?i)^rfMRI.*", 
	DMRI_RGX = "(?i)^.*dMRI.*",
	SBREF_RGX = "(?i)^.*_SBRef$",
	MISC_RGX = "(?i)^.*(resolve|Report|Perfusion|mIP|SWI|_MIP_|Physio|Localizer|AAHead|Scout).*"
	;
	

	/** The Constant PHYSIO_CHECK_SHORT_NAME. */
	public static final String PHYSIO_CHECK_SHORT_NAME = "PHYSIO_CHECK";
	
	/** The Constant PHYSIO_CHECK_LONG_NAME. */
	public static final String PHYSIO_CHECK_LONG_NAME = "Has physio files?";

	/** The Constant TASK_FILE_CHECK_SHORT_NAME. */
	public static final String TASK_FILE_CHECK_SHORT_NAME = "TASK_FILE_CHECK";
	
	/** The Constant TASK_FILE_CHECK_LONG_NAME. */
	public static final String TASK_FILE_CHECK_LONG_NAME = "Has task files (TAB.txt, .edat2)";
	
	private List<String> MAIN_SCAN_TYPES = Arrays.asList("rfMRI","tfMRI");

	/** The main scan types. */
	private JdbcTemplate _jdbcTemplate;

	/**
	 * Instantiates a new ccf CBA checker.
	 */
	@Autowired
	@Lazy
	public CcfCbaChecker(JdbcTemplate jdbcTemplate) {
		super();
		_jdbcTemplate = jdbcTemplate;
		
		_seriesDescNiftiCheckExclusionRegexes.add(MISC_RGX);
		_scanTypeNiftiCheckExclusionRegexes.add(MISC_RGX);
		_seriesDescFrameCountExclusionRegexes.add(MISC_RGX);
		_scanTypeFrameCountExclusionRegexes.add(MISC_RGX);
	}

	@Override
	public void initializeScanTypeNiftiFileCountMap() {
        _scanTypeNiftiFileCountMap.put("AAHScout", new Integer[] {2, 6, 8});
        _scanTypeNiftiFileCountMap.put("FieldMap_SE_EPI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("Bias_Receive", new Integer[] {2, 6, 8});
        _scanTypeNiftiFileCountMap.put("Localizer", new Integer[] {6, 8});
        _scanTypeNiftiFileCountMap.put("Localizer_aligned", new Integer[] {6, 8});
        _scanTypeNiftiFileCountMap.put("T1w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("rfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("rfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("tfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("tfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("dMRI", new Integer[] {4});
	// 2023/01/02:  dMRI SBRefs now sometimes have bvec/bval files.  Okaying "4" as a valid count, in addition to "2"
	// Per MH, it's okay for SBRefs to have bvec/bval files.
        _scanTypeNiftiFileCountMap.put("dMRI_SBRef", new Integer[] {2,4});
	}
	
	/* (non-Javadoc)
	 * @see org.nrg.ccf.sanitychecks.inter.SanityCheckerI#doChecks(java.lang.String, org.nrg.xdat.model.XnatImagesessiondataI, org.nrg.xft.security.UserI)
	 */
	@Override
	public void doNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user, List<SanityCheck> checkList) {
		

		log.debug("Run CCF CBA Sanity Checks");
		
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			final String scanType = scan.getType();
			if ((scanType != null && !MAIN_SCAN_TYPES.contains(scanType))) {
				continue;
			}
			doPhysioCheck(scan, checkList);
			doTaskFileCheck(scan, checkList);
		}
		

	}
	

	/**
	 * Do physio ev check.
	 *
	 * @param scan the scan
	 * @param checkList the check list
	 */
	protected void doPhysioCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {

		final SanityCheck sCheck = new SanityCheck(scan);
		sCheck.setOutcome(Outcome.PASSED);
		sCheck.setCheckLongName(PHYSIO_CHECK_LONG_NAME);
		sCheck.setCheckShortName(PHYSIO_CHECK_SHORT_NAME);
		XnatResourcecatalog linkedDataResource = null;
		final StringBuilder probSB = new StringBuilder();
		final String scanQuality = scan.getQuality();
		// FOR CBA:  Make physio a warning.  We are not generating it.  It was often not collected or not 
		// uploaded to the ITK project
		if (!(scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE"))) {
			if (!(physioFileCount(scan,linkedDataResource)>2)) {
				if (!probSB.toString().isEmpty()) {
					probSB.append(",");
				}
				probSB.append("Missing generated physio file(s)");
				sCheck.setOutcome(Outcome.WARNING);
			}
		} else {
			sCheck.setOutcome(Outcome.SCAN_UNUSABLE);
		}
		if (sCheck.getOutcome().equals(Outcome.FAILED) ||sCheck.getOutcome().equals(Outcome.WARNING)) {
			sCheck.setDescriptionOfFailure(probSB.toString());
		}
		checkList.add(sCheck);
	} 
	
	/**
	 * Do task file check.
	 *
	 * @param scan the scan
	 * @param checkList the check list
	 */
	protected void doTaskFileCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {

		final String currSeriesDesc = scan.getSeriesDescription();
		if (currSeriesDesc == null || !currSeriesDesc.matches(TFMRI_RGX)) { 
			return;
		}
		final SanityCheck sCheck = new SanityCheck(scan);
		sCheck.setOutcome(Outcome.PASSED);
		sCheck.setCheckLongName(TASK_FILE_CHECK_LONG_NAME);
		sCheck.setCheckShortName(TASK_FILE_CHECK_SHORT_NAME);
		XnatResourcecatalog linkedDataResource = null;
		final StringBuilder probSB = new StringBuilder();
		final String scanQuality = scan.getQuality();
		if (!(scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE"))) {
			if (!(taskFileCount(scan,linkedDataResource)>1)) {
				if (!probSB.toString().isEmpty()) {
					probSB.append(",");
				}
				probSB.append("Missing task timing file(s)");
				sCheck.setOutcome(Outcome.FAILED);
			}
		} else {
			sCheck.setOutcome(Outcome.SCAN_UNUSABLE);
		}
		if (sCheck.getOutcome().equals(Outcome.FAILED)) {
			sCheck.setDescriptionOfFailure(probSB.toString());
		}
		checkList.add(sCheck);
	} 
	

	/**
	 * Checks for wide csv and ev.
	 *
	 * @param scan the scan
	 * @param linkedDataResource the linked data resource
	 * @return true, if successful
	 */
	private Integer physioFileCount(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return 0;
			}
		}
		int count=0;
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.toLowerCase().endsWith(".log")) {
				if (fp.toLowerCase().matches("(?i)^.*(_puls|_ext|_info|_resp|ecg).*$")) {
					count+=1;
				}
			}
		}
		return count;
	}

	/**
	 * Checks for wide csv and ev.
	 *
	 * @param scan the scan
	 * @param linkedDataResource the linked data resource
	 * @return true, if successful
	 */
	private Integer taskFileCount(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return 0;
			}
		}
		int count=0;
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.endsWith("TAB.txt") || fp.endsWith(".edat2")) {
				count+=1;
			}
		}
		if (count>0) {
			// Only count .txt files if we have tab.txt or .edat2 (In case there are misc other txt files)
			for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
				final File f = cFile.getF();
				final String fp = f.getAbsolutePath();
				if (!fp.endsWith("TAB.txt") && fp.endsWith(".txt")) {
					count+=1;
				}
			}
		}
		return count;
	}
	

	/**
	 * Initialize linked data resource.
	 *
	 * @param scan the scan
	 * @return the xnat resourcecatalog
	 */
	private XnatResourcecatalog initializeLinkedDataResource(final XnatImagescandataI scan) {
		for (final XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			if (resource.getLabel().equals(CommonConstants.LINKED_DATA_RESOURCE)) {
				return (XnatResourcecatalog)resource;
			}
		}
		return null;
	}
	
}
