package org.nrg.ccf.sanitychecks.components.checkers.project;

import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.FacemaskingChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.FunctionalQcChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.ResourceVerificationChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.ValidationFailureChecker;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@SanityChecker(description = "Java-based sanity checks for CCF_HCD_ITK")
public class CcfHcdNewChecker extends CcfHcaNewChecker {
	
	@Autowired
	public CcfHcdNewChecker(ValidationFailureChecker validationFailureChecker, FunctionalQcChecker functionalQcChecker, 
			FacemaskingChecker facemaskingChecker, ResourceVerificationChecker resourceVerificationChecker) {
		super(validationFailureChecker, functionalQcChecker, facemaskingChecker, resourceVerificationChecker);
	}

	@Override
	public void doNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user, 
			List<SanityCheck> checkList) throws SanityChecksException {
		
		log.debug("Run CCF HCD Sanity Checks");
		
		checkList.addAll(_validationFailureChecker.doChecks(projectId, imageSession, user));
		checkList.addAll(_facemaskingChecker.doChecks(projectId, imageSession, user, 
				Arrays.asList(new String[] { "T1w_Norm_4e", "T1w_4e", "T1w_Norm", "T1w", "T2w", "T2w_Norm" })));
		checkList.addAll(_functionalQcChecker.doChecks(projectId, imageSession, user));
		checkList.addAll(_resourceVerificationChecker.doChecks(projectId, imageSession, user));
		
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			doScanLevelLinkedDataChecks(scan, checkList);
			doSufficientFramesCheck(scan, checkList);
			doPhysioEvChecks(scan, checkList);
		}
		
		doSessionLevelLinkedDataChecks(imageSession, checkList);
		
	}

	
	@Override
	protected void doSufficientFramesCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		//log.debug("Running frames check (SCAN=" + scan.getId() + ")");
        final String currSeriesDesc = scan.getSeriesDescription();
        if (currSeriesDesc == null || currSeriesDesc.matches(SBREF_RGX) || currSeriesDesc.toUpperCase().contains("PHYSIO")) {
                return;
        }
        if (currSeriesDesc.matches(RFMRI_RGX) || currSeriesDesc.matches(TFMRI_RGX)) { 
        	
        	SanityCheck sCheck = new SanityCheck(scan);
        	sCheck.setCheckLongName(COMPLETION_CHECK_LONG_NAME);
        	sCheck.setCheckShortName(COMPLETION_CHECK_SHORT_NAME);
            if (scan.getFrames() == null) {
            	sCheck.setOutcome(Outcome.FAILED);
            	sCheck.setDescriptionOfFailure("Could not determine frame count");
            }
            int frames = scan.getFrames().intValue();
            int frames10 = frames - 10;
            //String scanComplete = null;
            String scan100 = null;
            double scanPct = 0;
            if (currSeriesDesc.matches(RFMRI_RGX)) { 
            	//scanComplete = (frames>=488) ? "Y" : "N";
           		scanPct = (double)frames10/(488-10);
            	scan100 = (frames10>=100) ? "Y" : "N";
            	if (scan100.equals("N")) {
            		sCheck.setOutcome(Outcome.FAILED);
            		sCheck.setDescriptionOfFailure("Insufficient frames (" + frames + ")");
            	} else {
            		sCheck.setOutcome(Outcome.PASSED);
            	}
            } else if (currSeriesDesc.matches(TFMRI_RGX)) { 
            	if (currSeriesDesc.contains("_GUESSING")) {
            		//scanComplete = (frames>=280) ? "Y" : "N";
            		scanPct = (double)frames10/(280-10);
            	} else if (currSeriesDesc.contains("_CARIT")) {
            		//scanComplete = (frames>=300) ? "Y" : "N";
            		scanPct = (double)frames10/(300-10);
            	} else if (currSeriesDesc.contains("_EMOTION")) {
            		//scanComplete = (frames>=178) ? "Y" : "N";
            		scanPct = (double)frames10/(178-10);
            	} 
            	if (scanPct<.75) {
            		sCheck.setOutcome(Outcome.FAILED);
            		sCheck.setDescriptionOfFailure("Insufficient frames (" + frames + ")");
            	} else {
            		sCheck.setOutcome(Outcome.PASSED);
            	}
            } 
            if (sCheck.getOutcome() != null) {
            	checkList.add(sCheck);
            }
        } 
        
	}

}
