package org.nrg.ccf.sanitychecks.components.checkers.project;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sanitychecks.abst.AbstractNiftiCheckerSanityChecker;
import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class CcfPhcpChecker to execute sanity checks for CCF_PHCP_PRC project.
 * @author Atul
 */

@Slf4j
@Lazy
@Component
@Primary
@SanityChecker(description = "Java-based sanity checks for CCF_PHCP_PRC")
public class CcfPhcpChecker extends AbstractNiftiCheckerSanityChecker {

	/** The Constant COMPLETION_CHECK_SHORT_NAME. */
	public static final String COMPLETION_CHECK_SHORT_NAME = "COMPLETION_CHECK";

	/** The Constant COMPLETION_CHECK_LONG_NAME. */
	public static final String COMPLETION_CHECK_LONG_NAME = "Enough frames to release?";

	/** The Constant PHYSIO_CHECK_SHORT_NAME. */
	public static final String PHYSIO_CHECK_SHORT_NAME = "PHYSIO_CHECK";
	
	/** The Constant PHYSIO_CHECK_LONG_NAME. */
	public static final String PHYSIO_CHECK_LONG_NAME = "Physio files generated?";

	/** The Constant EV_CHECK_SHORT_NAME. */
	public static final String EV_CHECK_SHORT_NAME = "EV_CHECK";
	
	/** The Constant EV_CHECK_LONG_NAME. */
	public static final String EV_CHECK_LONG_NAME = "EV files generated/uploaded (PSYCHOPY/EVs folder)?";

	/** The Constant SBREF_RGX. */
	public static final String 
	TFMRI_RGX = "(?i)^.*fMRI_[^r].*", 
	RFMRI_RGX = "(?i)^.*fMRI_r.*", 
	DMRI_RGX = "(?i)^.*dMRI.*",
	SBREF_RGX = "(?i)^.*_SBRef$" 
	;

	/** The main scan types. */
	private List<String> MAIN_SCAN_TYPES = Arrays.asList("rfMRI","tfMRI");
	private JdbcTemplate _jdbcTemplate;
	
	@Override
	// NOTE:  This method is a modification of the validate method in the Dcm2NiiValidator class 
	public final List<SanityCheck> doChecks(String projectId, XnatImagesessiondataI imageSession, UserI user) 
			throws SanityChecksException {
		final String lcLabel = imageSession.getLabel().toLowerCase();
		final String scanner = imageSession.getScanner();
		final String sessionType = imageSession.getSessionType();
		boolean isMRS = scanner.contains("7TAS") && (lcLabel.contains("mrs") || sessionType.endsWith("MRS"));
		if (isMRS) {
			throw new SanityChecksException(
				"ERROR:  This session appears to be an MRS session.  Sanity checks are not yet supported for PHCP MRS sessions."
			);
		}
		return super.doChecks(projectId,  imageSession,  user);
	}

	/**
	 * Instantiates a new ccf PHCP checker.
	 */
	@Autowired
	@Lazy
	public CcfPhcpChecker(JdbcTemplate jdbcTemplate) {
		super();
		_jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public void initializeScanTypeNiftiFileCountMap() {
        _scanTypeNiftiFileCountMap.put("AAHScout", new Integer[] {2, 6, 8});
        _scanTypeNiftiFileCountMap.put("FieldMap", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("FieldMap_SE_EPI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("Localizer", new Integer[] {6});
        _scanTypeNiftiFileCountMap.put("Localizer_aligned", new Integer[] {6});
        _scanTypeNiftiFileCountMap.put("T1w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_4e", new Integer[] {8});
        _scanTypeNiftiFileCountMap.put("T1w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_Norm_4e", new Integer[] {8});
        _scanTypeNiftiFileCountMap.put("T1w_setter", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_setter", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("rfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("rfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("tfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("tfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("dMRI", new Integer[] {4});
	// 2023/01/02:  dMRI SBRefs now sometimes have bvec/bval files.  Okaying "4" as a valid count, in addition to "2"
	// Per MH, it's okay for SBRefs to have bvec/bval files.
        _scanTypeNiftiFileCountMap.put("dMRI_SBRef", new Integer[] {2,4});
	}
	
	@Override
	protected void initializeInitialFramesMaps(String projectId) {
		super.initializeInitialFramesMaps(projectId);
	}

	
	@Override
	public void doNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user, List<SanityCheck> checkList) {

		log.debug("Run CCF MDD Sanity Checks");
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			final String scanType = scan.getType();
			if ((scanType != null && !MAIN_SCAN_TYPES.contains(scanType))) {
				continue;
			}
			doSufficientFramesCheck(scan, checkList);
			doPhysioCheck(scan, checkList);
			doEvCheck(scan, checkList);
		}
		
	}

	/**
	 * Do physio ev check.
	 *
	 * @param scan the scan
	 * @param checkList the check list
	 */
	protected void doPhysioCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {

		final SanityCheck sCheck = new SanityCheck(scan);
		sCheck.setOutcome(Outcome.PASSED);
		sCheck.setCheckLongName(PHYSIO_CHECK_LONG_NAME);
		sCheck.setCheckShortName(PHYSIO_CHECK_SHORT_NAME);
		XnatResourcecatalog linkedDataResource = null;
		final StringBuilder probSB = new StringBuilder();
		final String scanQuality = scan.getQuality();
		if (!(scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE"))) {
			if (!hasPhysio(scan,linkedDataResource)) {
				if (!probSB.toString().isEmpty()) {
					probSB.append(",");
				}
				probSB.append("Missing generated physio file(s)");
				sCheck.setOutcome(Outcome.FAILED);
			}
		} else {
			sCheck.setOutcome(Outcome.SCAN_UNUSABLE);
		}
		if (sCheck.getOutcome().equals(Outcome.FAILED)) {
			sCheck.setDescriptionOfFailure(probSB.toString());
		}
		checkList.add(sCheck);
	} 
	
	/**
	 * Do ev check.
	 *
	 * @param scan the scan
	 * @param checkList the check list
	 */
	protected void doEvCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {

		final String currSeriesDesc = scan.getSeriesDescription();
		if (currSeriesDesc == null || !currSeriesDesc.matches(TFMRI_RGX)) { 
			return;
		}
		final SanityCheck sCheck = new SanityCheck(scan);
		sCheck.setOutcome(Outcome.PASSED);
		sCheck.setCheckLongName(EV_CHECK_LONG_NAME);
		sCheck.setCheckShortName(EV_CHECK_SHORT_NAME);
		XnatResourcecatalog linkedDataResource = null;
		final StringBuilder probSB = new StringBuilder();
		final String scanQuality = scan.getQuality();
		if (!(scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE"))) {
			if (!hasEv(scan,linkedDataResource)) {
				if (!probSB.toString().isEmpty()) {
					probSB.append(",");
				}
				probSB.append("Missing EV file(s)");
				sCheck.setOutcome(Outcome.FAILED);
			}
		} else {
			sCheck.setOutcome(Outcome.SCAN_UNUSABLE);
		}
		if (sCheck.getOutcome().equals(Outcome.FAILED)) {
			sCheck.setDescriptionOfFailure(probSB.toString());
		}
		checkList.add(sCheck);
	} 

	/**
	 * Checks for wide csv and ev.
	 *
	 * @param scan the scan
	 * @param linkedDataResource the linked data resource
	 * @return true, if successful
	 */
	private boolean hasPhysio(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}

		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("/PHYSIO/") && fp.toLowerCase().endsWith(".csv")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks for wide csv and ev.
	 *
	 * @param scan the scan
	 * @param linkedDataResource the linked data resource
	 * @return true, if successful
	 */
	private boolean hasEv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}

		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("PSYCHOPY/EVs/") && fp.contains(".txt")) {
				return true;
			}
		}
		return false;
	}
	
	protected void doSufficientFramesCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		log.debug("Running frames check (SCAN=" + scan.getId() + ")");
        final String currSeriesDesc = scan.getSeriesDescription();
       	if (currSeriesDesc == null || currSeriesDesc.matches(SBREF_RGX) || currSeriesDesc.toUpperCase().contains("PHYSIO")) {
                return;
        }
        if (currSeriesDesc.matches(RFMRI_RGX) || currSeriesDesc.matches(TFMRI_RGX)) { 
        	
        	SanityCheck sCheck = new SanityCheck(scan);
        	sCheck.setCheckLongName(COMPLETION_CHECK_LONG_NAME);
        	sCheck.setCheckShortName(COMPLETION_CHECK_SHORT_NAME);
            if (scan.getFrames() == null) {
            	sCheck.setOutcome(Outcome.FAILED);
            	sCheck.setDescriptionOfFailure("Could not determine frame count");
            }
            int frames = scan.getFrames().intValue();
            int frames10 = frames - 10;
            //String scanComplete = null;
            String scan100 = null;
            double scanPct = 0;
            if (currSeriesDesc.matches(RFMRI_RGX)) { 
            	//scanComplete = (frames>=488) ? "Y" : "N";
           		scanPct = (double)frames10/(488-10);
            	scan100 = (frames10>=100) ? "Y" : "N";
            	if (scan100.equals("N")) {
            		sCheck.setOutcome(Outcome.FAILED);
            		sCheck.setDescriptionOfFailure("Insufficient frames (" + frames + ")");
            	} else {
            		sCheck.setOutcome(Outcome.PASSED);
            	}
            } else if (currSeriesDesc.matches(TFMRI_RGX)) { 
            	if (currSeriesDesc.contains("_SOC")) {
            		//scanComplete = (frames>=194) ? "Y" : "N";
            		scanPct = (double)frames10/(295-10);
            	} else if (currSeriesDesc.contains("_DPX")) {
            		//scanComplete = (frames>=300) ? "Y" : "N";
            		scanPct = (double)frames10/(450-10);
            	} else if (currSeriesDesc.contains("_MID")) {
            		//scanComplete = (frames>=345) ? "Y" : "N";
            		scanPct = (double)frames10/(1010-10);
            	} 
            	if (scanPct<.75) {
            		sCheck.setOutcome(Outcome.FAILED);
            		sCheck.setDescriptionOfFailure("Insufficient frames (" + frames + ")");
            	} else {
            		sCheck.setOutcome(Outcome.PASSED);
            	}
            } 
            if (sCheck.getOutcome() != null) {
            	checkList.add(sCheck);
            }
        } 
        
	}

	/**
	 * Initialize linked data resource.
	 *
	 * @param scan the scan
	 * @return the xnat resourcecatalog
	 */
	private XnatResourcecatalog initializeLinkedDataResource(final XnatImagescandataI scan) {
		for (final XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			if (resource.getLabel().equals(CommonConstants.LINKED_DATA_RESOURCE)) {
				return (XnatResourcecatalog)resource;
			}
		}
		return null;
	}
	
}
