package org.nrg.ccf.sanitychecks.components.checkers.project;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.components.abst.AbstractCcfStagingSanityChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.ResourceVerificationChecker;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.ccf.sessionbuilding.exception.SessionBuildingException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.pojo.ProcessingResults;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.ccf.sessionbuilding.utils.CcfSessionBuildingUtils;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@SanityChecker(description = "Java-based sanity checks for CCF_MDD_STG")
public class CcfMddStagingChecker extends AbstractCcfStagingSanityChecker {
	
   	public static final String HAS_LINKEDDATA_CHECK_SHORT_NAME = "linked_data";
   	public static final String HAS_LINKEDDATA_CHECK_LONG_NAME = "Has Linked data resource?";
   	
   	public static final String HAS_EV_CHECK_SHORT_NAME = "EV_CHECK";
   	public static final String HAS_EV_CHECK_LONG_NAME = "Has EV files?";
   	
   	public static final String HAS_PHYSIO_CHECK_SHORT_NAME = "has_physiocsv";
   	public static final String HAS_PHYSIO_CHECK_LONG_NAME = "Has physio csv file?";
   	
   	public static final String HAS_CARITDESIGN_CHECK_SHORT_NAME = "has_carit_design";
   	public static final String HAS_CARITDESIGN_CHECK_LONG_NAME = "Has carit design csv file?";

	public static final String 
			TFMRI_RGX = "(?i)^tfMRI.*", 
			RFMRI_RGX = "(?i)^rfMRI.*", 
			DMRI_RGX = "(?i)^dMRI.*",
			SBREF_RGX = "(?i)^.*_SBRef$", 
			CARIT_RGX = "(?i)^.*CARIT.*$", 
			FACEMATCH_RGX = "(?i)^.*FACEMATCH.*$", 
			PHYSIOCSV_RGX = "(?i)^.*Physio_.*csv$", 
			CARITDESIGN_RGX = "(?i)^.*CARIT.*design.*csv$" 
			;
	
	protected List<String> MAIN_SCAN_TYPES = Arrays.asList(new String[] {"rfMRI","tfMRI","dMRI"});
	protected List<String> LINKED_DATA_REGEXES = Arrays.asList(new String[] {"^rfMRI$","^tfMRI$"});

	@Lazy
	@Autowired
	public CcfMddStagingChecker(ResourceVerificationChecker resourceVerificationChecker, CcfReleaseRulesUtils releaseRulesUtils) {
		super(resourceVerificationChecker, releaseRulesUtils);
	}

	@Override
	public void initializeSourceProjectValue() {
		SOURCE_PROJECTS = new String[] {"CCF_MDD_PRC"};
	}

	@Override
	public void doNonStagingNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user,
			List<SanityCheck> checkList) throws SanityChecksException {
		log.debug("Run CCF MDD Staging Sanity Checks");
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			doScanLevelLinkedDataChecks(scan, checkList);
			doEvCheck(scan, checkList);
		}
	}
	
	@Override
	public String getSelectionCriteriaFromSession(XnatImagesessiondataI session) {
		return "ALL";
	}

	@Override
	public void initializeScanTypeNiftiFileCountMap() {
        _scanTypeNiftiFileCountMap.put("AAHScout", new Integer[] {2, 6, 8});
        _scanTypeNiftiFileCountMap.put("FieldMap_SE_EPI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("Bias_Receive", new Integer[] {2, 6, 8});
        _scanTypeNiftiFileCountMap.put("Localizer", new Integer[] {6, 8});
        _scanTypeNiftiFileCountMap.put("Localizer_aligned", new Integer[] {6, 8});
        _scanTypeNiftiFileCountMap.put("T1w_setter", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_4e", new Integer[] {8});
        _scanTypeNiftiFileCountMap.put("T1w_Norm_4e", new Integer[] {8});
        _scanTypeNiftiFileCountMap.put("T1w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_setter", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("rfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("rfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("tfMRI", new Integer[] {2, 3});
        _scanTypeNiftiFileCountMap.put("tfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("dMRI", new Integer[] {4});
	// 2023/01/02:  dMRI SBRefs now sometimes have bvec/bval files.  Okaying "4" as a valid count, in addition to "2"
	// Per MH, it's okay for SBRefs to have bvec/bval files.
        _scanTypeNiftiFileCountMap.put("dMRI_SBRef", new Integer[] {2,4});
        _scanTypeNiftiFileCountMap.put("mbPCASLhr", new Integer[] {2});
	}

	@Override
	public void initializeSeriesDescNiftiFileCountMap() {
        _seriesDescNiftiFileCountMap.put("tfMRI_CARIT_PA", new Integer[] {3});
        _seriesDescNiftiFileCountMap.put("tfMRI_FACEMATCHING_AP", new Integer[] {2});
        _seriesDescNiftiFileCountMap.put("tfMRI_FACEMATCHING_PA", new Integer[] {2});
	}
	

	protected void doScanLevelLinkedDataChecks(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		final String scanQuality = scan.getQuality();
        final String currScanType = scan.getType();
        final String currSeriesDesc = scan.getSeriesDescription();
        boolean isLinkedDataScan = false;
        for (final String regex : LINKED_DATA_REGEXES) {
        	if (currScanType.matches(regex)) {
        		isLinkedDataScan = true;
        	}
		}
		if ((!isLinkedDataScan) || currScanType.matches(SBREF_RGX) || scanQuality.equalsIgnoreCase("UNUSABLE")) {
			return;
		}
        final SanityCheck sCheck = new SanityCheck(scan);
        sCheck.setOutcome(Outcome.PASSED);
        sCheck.setCheckShortName(HAS_LINKEDDATA_CHECK_SHORT_NAME);
        sCheck.setCheckLongName(HAS_LINKEDDATA_CHECK_LONG_NAME);
        final XnatResourcecatalog linkedDataResource = ResourceUtils.getResource(scan, CommonConstants.LINKED_DATA_RESOURCE);
        if (linkedDataResource == null) {
        	sCheck.setOutcome(Outcome.FAILED);
        	sCheck.setDescriptionOfFailure("Linked data not uploaded");
        	checkList.add(sCheck);
        	// To be consistent with earlier automation-based checks, let's just skip individual file checks
        	// when we don't have a linked data resource
        	return;
        } else { 
        	checkList.add(sCheck);
        }
        
		if (currScanType.matches(RFMRI_RGX) || currScanType.matches(TFMRI_RGX)) {
			
			final SanityCheck physioCheck = new SanityCheck(scan);
			physioCheck.setOutcome(Outcome.PASSED);
			physioCheck.setCheckShortName(HAS_PHYSIO_CHECK_SHORT_NAME);
			physioCheck.setCheckLongName(HAS_PHYSIO_CHECK_LONG_NAME);
			if (!hasMatchingFile(linkedDataResource,PHYSIOCSV_RGX )) {
				physioCheck.setOutcome(Outcome.FAILED);
				physioCheck.setDescriptionOfFailure("Physio csv file does not exist in resource.");
			}
			checkList.add(physioCheck);
			
		}
		if (currScanType.matches(TFMRI_RGX)) {
			
			if (currSeriesDesc.matches(CARIT_RGX)) {
			
				final SanityCheck designCheck = new SanityCheck(scan);
				designCheck.setOutcome(Outcome.PASSED);
				designCheck.setCheckShortName(HAS_CARITDESIGN_CHECK_SHORT_NAME);
				designCheck.setCheckLongName(HAS_CARITDESIGN_CHECK_LONG_NAME);
				if (!hasMatchingFile(linkedDataResource,CARITDESIGN_RGX)) {
					designCheck.setOutcome(Outcome.FAILED);
					designCheck.setDescriptionOfFailure("Task csv does not exist in resource.");
				}
				checkList.add(designCheck);
				
			}
			
		}
        
    } 
	
	protected void doEvCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		final String scanType = scan.getType();
		final String scanQuality = scan.getQuality();
        final String currSeriesDesc = scan.getSeriesDescription();
       	if (!scanType.matches(TFMRI_RGX) || scanType.matches(SBREF_RGX)) {
			return;
       	}
		if (scanQuality.equalsIgnoreCase("UNUSABLE")) {
			return;
		}
        SanityCheck sCheck = new SanityCheck(scan);
        sCheck.setOutcome(Outcome.PASSED);
        sCheck.setCheckLongName(HAS_EV_CHECK_LONG_NAME);
        sCheck.setCheckShortName(HAS_EV_CHECK_SHORT_NAME);
        XnatResourcecatalog linkedDataResource = null;
        final StringBuilder probSB = new StringBuilder();
        if (currSeriesDesc.matches(TFMRI_RGX)) { 
        	if (!hasEv(scan,linkedDataResource)) {
        		if (!probSB.toString().isEmpty()) {
        			probSB.append(",");
        		}
        		probSB.append("Missing EV file(s)");
        		sCheck.setOutcome(Outcome.FAILED);
        	}
        }
        if (sCheck.getOutcome().equals(Outcome.FAILED)) {
        	sCheck.setDescriptionOfFailure(probSB.toString());
        }
       	checkList.add(sCheck);
    } 
	
	private boolean hasMatchingFile(XnatResourcecatalog linkedDataResource, String rgx) {
		if (linkedDataResource == null) {
			return false;
		}
		List<String> fileNames = ResourceUtils.getFileNames(linkedDataResource);
		for (final String fName : fileNames) {
			if (fName.matches(rgx)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean hasEv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("/EVs/")) {
				return true;
			}
		}
		return false;
	}
	
	private XnatResourcecatalog initializeLinkedDataResource(final XnatImagescandataI scan) {
		for (final XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			if (resource.getLabel().equals(CommonConstants.LINKED_DATA_RESOURCE)) {
				return (XnatResourcecatalog)resource;
			}
		}
		return null;
	}
	

	@Override
	protected boolean compareCurrentWithRebuiltSession(XnatImagesessiondataI imageSession, UserI user) throws Exception {
		
		if (imageSession == null || !(imageSession instanceof XnatMrsessiondata)) {
			throw new SessionBuildingException("Source session must be an MR Session");
		}
		XnatMrsessiondata mrSession = (XnatMrsessiondata)imageSession;
		final XnatProjectdata sourceProj = XnatProjectdata.getXnatProjectdatasById(SOURCE_PROJECTS[0], user, false);
		if (sourceProj == null) {
			throw new SessionBuildingException("Could not obtain source project (" + SOURCE_PROJECTS[0] + ")");
		}
		XnatSubjectdata sourceSubj = null;
    	final CriteriaCollection cc=new CriteriaCollection("OR");
        cc.addClause("xnat:subjectData/project", sourceProj.getId());
		final List<XnatSubjectdata> subjectList = XnatSubjectdata.getXnatSubjectdatasByField(cc, user, false);
		int listLength = subjectList.size();
		System.out.println(listLength);
		final String compLabel = imageSession.getLabel().replaceFirst("_MR","").replaceAll("_", "");
		final String altCompLabel = imageSession.getLabel().replaceAll("_.*$","");
		for (final XnatSubjectdata subj : subjectList) {
			final String subjLabel = subj.getLabel().toLowerCase().replaceAll("_", "");
			if (subjLabel.equals(compLabel) || subjLabel.equals(altCompLabel)) {
				sourceSubj=subj;
				break;
			}
		}
		if (sourceSubj == null) {
			throw new SessionBuildingException("Could not obtain source project subject");
		}
		final CcfReleaseRulesI releaseRules = _releaseRulesUtils.getReleaseRulesForProject(SOURCE_PROJECTS[0]).getReleaseRules();
		if (releaseRules == null) {
			throw new SessionBuildingException("Could not obtain source project release rules class");
		}
		final Map<String,String> paramMap = new HashMap<>();
		paramMap.put("errorOverride", "true");
		String selectionCriteria = imageSession.getLabel().replaceFirst("_MR","").replaceAll("^.*_", "");
		if (selectionCriteria != null) {
			paramMap.put("selectionCriteria", selectionCriteria);
		}
		final ProcessingResults rebuildResults = CcfSessionBuildingUtils.processSubject(sourceProj, sourceSubj, releaseRules, paramMap, user);
		final List<XnatMrsessiondata> rebuildSessionList = rebuildResults.getSessionList();
		if (rebuildSessionList == null || rebuildSessionList.size()<1) {
			throw new SessionBuildingException("Processing returned no experiments");
		}
		final XnatMrsessiondata rebuildSession = CcfSessionBuildingUtils.buildAndReturnCombinedSession(sourceProj, sourceSubj, paramMap, releaseRules, rebuildSessionList, user);
		if (rebuildSession == null) {
			throw new SessionBuildingException("Could not generate rebuilt session");
		}
		Map<String,String> rebuildMap = new TreeMap<>(getScanMap(rebuildSession));
		Map<String,String> currentMap = new TreeMap<>(getScanMap(mrSession));
		if (rebuildSession.getScans_scan().size() != mrSession.getScans_scan().size()) {
			log.error("Session rebuild check failed (SESSION=" + mrSession.getLabel() + "): \nSTAGING SESSION:  " +
					currentMap.toString() + "\nREBUILD SESSION: " + rebuildMap.toString());
			return false;
		};
		for (Entry<String, String> entry : rebuildMap.entrySet()) {
			if (!currentMap.containsKey(entry.getKey()) || !entry.getValue().equals(currentMap.get(entry.getKey()))) {
				log.error("Session rebuild check failed (SESSION=" + mrSession.getLabel() + "): \nSTAGING SESSION:  " +
						currentMap.toString() + "\nREBUILD SESSION: " + rebuildMap.toString());
				return false;
			} 
		}
		return true;
	}

}
	
