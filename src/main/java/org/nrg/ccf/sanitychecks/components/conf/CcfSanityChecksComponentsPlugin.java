package org.nrg.ccf.sanitychecks.components.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "ccfSanityChecksComponentsPlugin",
			name = "CCF Sanity Checks Components Plugin",
			entityPackages = "org.nrg.ccf.pcp.entities",
			log4jPropertiesFile="/META-INF/resources/sanityCheckLog4j.properties"
		)
@ComponentScan({ 
		"org.nrg.ccf.sanitychecks.components.conf",
		"org.nrg.ccf.sanitychecks.components.checkers.shared",
		"org.nrg.ccf.sanitychecks.components.checkers.project"
	})
public class CcfSanityChecksComponentsPlugin {
	
	public static Logger logger = Logger.getLogger(CcfSanityChecksComponentsPlugin.class);

	public CcfSanityChecksComponentsPlugin() {
		logger.info("Configuring the CCF Sanity Checks Components plugin.");
	}
	
}
