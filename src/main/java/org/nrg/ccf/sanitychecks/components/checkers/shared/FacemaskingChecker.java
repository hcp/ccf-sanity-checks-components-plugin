package org.nrg.ccf.sanitychecks.components.checkers.shared;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.sanitychecks.constants.CheckResult;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.ccf.sanitychecks.inter.SanityCheckerRequiresInfoI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.springframework.stereotype.Component;

@Component
public class FacemaskingChecker implements SanityCheckerRequiresInfoI {
	
	public static final String FACEMASKING_CHECKS_SHORT_NAME = "facemask";
	public static final String FACEMASKING_CHECKS_LONG_NAME = "Facemask resource";
	
	@Override
	@SuppressWarnings("unchecked")
	public List<SanityCheck> doChecks(String projectId, XnatImagesessiondataI imageSession, UserI user, Object scanTypeList)
			throws SanityChecksException {
		final List<SanityCheck> checkList = new ArrayList<>();
		List<String> scantypes; 
		try {
			scantypes = (List<String>)scanTypeList;
		} catch (Exception e) {
			throw new SanityChecksException("Please provide a list of scan types", e);
		}
		
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			
			final String currScanType = scan.getType();
			if (!scantypes.contains(currScanType)) {
				continue;
			}
			final SanityCheck scanCheck = new SanityCheck(scan);
			scanCheck.setCheckLongName(FACEMASKING_CHECKS_LONG_NAME);
			scanCheck.setCheckShortName(FACEMASKING_CHECKS_SHORT_NAME);
			final CheckResult result = doFacemaskingChecks(scan, user);
			if (result == null) {
				continue;
			}
			final Outcome outcome = result.getOutcome();
			scanCheck.setOutcome(outcome);
			if (outcome.equals(Outcome.FAILED)) {
				scanCheck.setDescriptionOfFailure(result.getDescriptionOfFailure());
			}
			checkList.add(scanCheck);
			
		}
		return checkList;
		
	}

	private CheckResult doFacemaskingChecks(XnatImagescandataI scan, UserI user) {
		final CheckResult result = new CheckResult(Outcome.PASSED);
		final XnatResourcecatalog dicomResource = ResourceUtils.getResource(scan, CommonConstants.DICOM_RESOURCE);
		final XnatResourcecatalog dicomOrigResource = ResourceUtils.getResource(scan, CommonConstants.DICOM_ORIG_RESOURCE);
		if (dicomResource == null || dicomOrigResource == null) {
			result.setOutcome(Outcome.FAILED);
			result.setDescriptionOfFailure(String.format("Missing FaceMask Resources:  (DICOM=%s, DICOM_ORIG=%s)",
					(dicomResource != null) ? dicomResource.getLabel() : "NULL",
					(dicomOrigResource != null) ? dicomOrigResource.getLabel() : "NULL"));
			return result;
		}
		final Integer dicomCount = (dicomResource != null && dicomResource.getFileCount() != null) ?
				dicomResource.getFileCount() : 0;
		final Integer dicomOrigCount = (dicomOrigResource != null && dicomOrigResource.getFileCount() != null) ?
				dicomOrigResource.getFileCount() : 0;
		if (!dicomCount.equals(dicomOrigCount)) {
			result.setOutcome(Outcome.FAILED);
			result.setDescriptionOfFailure("DICOM and DICOM_ORIG have different file counts.");
			return result;
		}
		// Checked that DICOM resource contains files that appear to be facemasked files
		final List<File> dicomFiles = ResourceUtils.getFiles(dicomResource);
		for (final File dicomFile : dicomFiles) {
			if (!dicomFile.getName().matches("^[0-9][0-9][0-9]_.*$")) {
				result.setOutcome(Outcome.FAILED);
				result.setDescriptionOfFailure("DICOM resource contains files that appear not to be facemasked DICOM (by filename)");
				return result;
			}
		}
		return result;
	}

}
