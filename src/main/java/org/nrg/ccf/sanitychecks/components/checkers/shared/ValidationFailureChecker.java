package org.nrg.ccf.sanitychecks.components.checkers.shared;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.protocolval.exception.ProtocolValidationException;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.inter.SanityCheckerI;
import org.nrg.xdat.model.SanSanitychecksScanChecksCheckI;
import org.nrg.xdat.model.SanSanitychecksScanChecksI;
import org.nrg.xdat.model.ValProtocoldataConditionI;
import org.nrg.xdat.model.ValProtocoldataScanCheckConditionI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.SanSanitychecks;
import org.nrg.xdat.om.ValProtocoldata;
import org.nrg.xdat.om.ValProtocoldataScanCheck;
import org.nrg.xdat.om.XnatImageassessordata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ValidationFailureChecker implements SanityCheckerI {
	
	public static final String HAS_VALIDATION_SHORT_NAME = "HAS_VALIDATION";
	public static final String HAS_VALIDATION_LONG_NAME = "Has Protocol Validation Assessor";
	
	public static final String SESSION_VALIDATION_PASSED_SHORT_NAME = "VALIDATION_PASSED";
	public static final String SESSION_VALIDATION_PASSED_LONG_NAME = "Passed Session-Level Validation Checks";
	
	public static final String SCAN_VALIDATION_PASSED_SHORT_NAME = "SCAN_VALIDATION_PASSED";
	public static final String SCAN_VALIDATION_PASSED_LONG_NAME = "Passed Scan-Level Validation Checks";
	//public static final String[] IGNORED_VALIDATION_CHECKS = new String[] { };
	public static final String[] IGNORED_VALIDATION_CHECKS = new String[] {"Frames", "ImageComments", "Phase Encoding Rotation",
			"Reference Voltage Check", "Shim Check Advanced Mode", "Shim Check Standard Mode","Localizer Blocks Followed By Fieldmap Check"};
	
	public ValidationFailureChecker() {
		
	}
	
	final Comparator<ValProtocoldata> _comparator = new Comparator<ValProtocoldata>() {

			@Override
			public int compare(ValProtocoldata o1, ValProtocoldata o2) {
				final Object date1 = o1.getDate();
				final Object date2 = o2.getDate();
				if (date1 == null || date2 == null) {
					return (date1 == null && date2 == null) ? 0 : (date1 == null) ? -1 : 1;
				}
				if (date1 instanceof Date && date2 instanceof Date) {
					return ((Date)date1).compareTo((Date)date2);
				} else {
					return date1.toString().compareTo(date2.toString());
				}
			}
			
		};

	@Override
	public List<SanityCheck> doChecks(String projectId, XnatImagesessiondataI imageSession, UserI user) {
		
		log.debug("Run ValidationFailureChecker checks.");
		log.debug("Check session failure status");
		
		final ArrayList<SanityCheck> checkList = new ArrayList<>();
		final CriteriaCollection valCC = new CriteriaCollection("AND");
	      	valCC.addClause("val:ProtocolData.imageSession_ID",imageSession.getId());
	      	valCC.addClause("val:ProtocolData.project",projectId);
		final List<ValProtocoldata> currDatas = ValProtocoldata.getValProtocoldatasByField(valCC, user, false);
		
		final SanityCheck hasValidationCheck = new SanityCheck(imageSession);
		hasValidationCheck.setCheckLongName(HAS_VALIDATION_LONG_NAME);
		hasValidationCheck.setCheckShortName(HAS_VALIDATION_SHORT_NAME);
		checkList.add(hasValidationCheck);
		
		final SanityCheck validationPassedCheck = new SanityCheck(imageSession);
		validationPassedCheck.setCheckLongName(SESSION_VALIDATION_PASSED_LONG_NAME);
		validationPassedCheck.setCheckShortName(SESSION_VALIDATION_PASSED_SHORT_NAME);
		checkList.add(validationPassedCheck);
		ValProtocoldata valAssessor = null;
		Collections.sort(currDatas,_comparator);
		if (currDatas.size()>0) {
			hasValidationCheck.setOutcome(Outcome.PASSED);
			valAssessor = currDatas.get(currDatas.size()-1);
			final Outcome outcome = getSessionCheckOutcome(valAssessor);
			validationPassedCheck.setOutcome(outcome);
			if (outcome.equals(Outcome.FAILED)) {
				validationPassedCheck.setDescriptionOfFailure("Failed one or more session-level validation checks");
			}
		} else {
			hasValidationCheck.setOutcome(Outcome.FAILED);
			hasValidationCheck.setDescriptionOfFailure("Session has no ProtocolValidation assessor");
			validationPassedCheck.setOutcome(Outcome.FAILED);
			validationPassedCheck.setDescriptionOfFailure("Session has no ProtocolValidation assessor");
		}
		
		log.debug("Session failure status check complete");
		
		// NOTE: Not all scans have protocol checks.  When we don't have a validation assessor, we'll
		// only flag checks as failed if we have a sanity check record that already has a check
		SanSanitychecks sanityChecks = null;
		if (valAssessor == null && imageSession instanceof XnatImagesessiondata) {
			final ArrayList<XnatImageassessordata> assessors = ((XnatImagesessiondata)imageSession).getAssessors(SanSanitychecks.SCHEMA_ELEMENT_NAME);
			log.debug("Number of sanity checks assessors found:  " + assessors.size());
			if (assessors.size()>0) {
				sanityChecks = (SanSanitychecks)assessors.get(0);
			}
		}
		
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			
			final String scanId = scan.getId();
			final SanityCheck scanCheck = new SanityCheck(scan);
			scanCheck.setCheckLongName(SCAN_VALIDATION_PASSED_LONG_NAME);
			scanCheck.setCheckShortName(SCAN_VALIDATION_PASSED_SHORT_NAME);
			Outcome outcome;
			try {
				outcome = getScanCheckOutcome(valAssessor, scan, user, sanityChecks);
			} catch (ProtocolValidationException e) {
			    outcome = Outcome.FAILED;
			    scanCheck.setOutcome(outcome);
				scanCheck.setDescriptionOfFailure("Exception thrown obtaining protocol validation outcome (scan validation not produced for scan?)");
				checkList.add(scanCheck);
				return checkList;
			}
			log.debug("Check scan failure status (SCAN=" + scanId + ",OUTCOME=" + outcome + ")");
			if (outcome == null) {
				continue;
			}
			scanCheck.setOutcome(outcome);
			if (outcome.equals(Outcome.FAILED)) {
				scanCheck.setDescriptionOfFailure("Failed one or more scan-level validation checks");
			}
			checkList.add(scanCheck);
			
		}
		return checkList;
	}

	private Outcome getSessionCheckOutcome(ValProtocoldata valAssessor) {
		
		if (valAssessor == null) {
			return Outcome.FAILED;
		}
		//outerLoop:
		for (final ValProtocoldataConditionI condition : valAssessor.getCheck_conditions_condition()) {
			if (condition.getStatus().toUpperCase().contains("FAIL")) {
				if (Arrays.asList(IGNORED_VALIDATION_CHECKS).contains(condition.getId())) {
					continue;
				}
				return Outcome.FAILED;
			}
		}
		return Outcome.PASSED;
		
	}

	private Outcome getScanCheckOutcome(ValProtocoldata valAssessor, XnatImagescandataI scan, UserI user, SanSanitychecks sanityChecks) throws ProtocolValidationException {
		
		if (valAssessor == null) {
			// We'll only return an outcome if we have an assessor with a check for the scan.
			if (sanityChecks != null) {
				final List<SanSanitychecksScanChecksI> scanChecks = sanityChecks.getScans_scanChecks();
				for (final SanSanitychecksScanChecksI scanCheck : scanChecks) {
					if (scanCheck.getScanId().equals(scan.getId())) {
						List<SanSanitychecksScanChecksCheckI> checks = scanCheck.getCheck();
						for (final SanSanitychecksScanChecksCheckI check : checks) {
							if (check.getId().equals(SCAN_VALIDATION_PASSED_SHORT_NAME)) {
								return Outcome.FAILED;
							}
						}
					}
				}
			}
			return null;
		}
		
		final String scanId = scan.getId();
		final ValProtocoldataScanCheck valScan;
		try {
			valScan = valAssessor.getScanProtocolValidation(scanId);
		} catch (Exception e) {
			throw new ProtocolValidationException(e);
		}
		if (valScan == null) {
			return Outcome.PASSED;
		}
		final String scanStatus = valScan.getStatus().toUpperCase();
		if (scanStatus != null && scanStatus.toUpperCase().contains("FAIL")) {
			List<ValProtocoldataScanCheckConditionI> conditions = valScan.getConditions_condition();
			for (final ValProtocoldataScanCheckConditionI condition : conditions) {
				final String conditionStatus = condition.getStatus();
				final String conditionId = condition.getId();
				if (conditionStatus.toUpperCase().contains("FAIL") && !Arrays.asList(IGNORED_VALIDATION_CHECKS).contains(conditionId)) {
					return Outcome.FAILED;
				}
			}
		} else if (scanStatus != null && scanStatus.contains("NO CHECKS DEFINED")) {
			return null;
		}
		return Outcome.PASSED;
		
	}

}
