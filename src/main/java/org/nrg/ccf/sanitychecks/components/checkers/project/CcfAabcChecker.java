package org.nrg.ccf.sanitychecks.components.checkers.project;

import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.FacemaskingChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.FunctionalQcChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.ResourceVerificationChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.ValidationFailureChecker;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@SanityChecker(description = "Java-only (non-automation) sanity checks for CCF_AABC_ITK")
public class CcfAabcChecker extends CcfHcaNewChecker {
	
	@Override
	public void initializeScanTypeNiftiFileCountMap() {
        //_scanTypeNiftiFileCountMap.put("AAHScout", new Integer[] {2, 6, 8});
        //_scanTypeNiftiFileCountMap.put("FieldMap_SE_EPI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("Localizer", new Integer[] {6});
        _scanTypeNiftiFileCountMap.put("Localizer_aligned", new Integer[] {6});
        _scanTypeNiftiFileCountMap.put("T1w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_4e", new Integer[] {8});
        _scanTypeNiftiFileCountMap.put("T1w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_Norm_4e", new Integer[] {8});
        _scanTypeNiftiFileCountMap.put("T1w_setter", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_setter", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("TSE_HiResHp", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("TSE_Norm_HiResHp", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("rfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("rfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("tfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("tfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("dMRI", new Integer[] {4});
	// 2023/01/02:  dMRI SBRefs now sometimes have bvec/bval files.  Okaying "4" as a valid count, in addition to "2"
	// Per MH, it's okay for SBRefs to have bvec/bval files.
        _scanTypeNiftiFileCountMap.put("dMRI_SBRef", new Integer[] {2,4});
        _scanTypeNiftiFileCountMap.put("mbPCASLhr", new Integer[] {2});
		
	}

	@Override
	protected void doSessionLevelLinkedDataChecks(XnatImagesessiondataI imageSession, List<SanityCheck> checkList) {
		
		boolean hasRest = false,
				hasCarit = false,
				hasVismotor = false,
				hasGuessing = false,
				hasEmotion = false,
				hasFacename = false,
				hasPcasl = false,
				hasAny = false
		;
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			final String currSeriesDesc = scan.getSeriesDescription();
			final String currScanType = scan.getType();
			final String scanQuality = scan.getQuality();
			if (!(MAIN_SCAN_TYPES.contains(currScanType)) || currScanType.matches(SBREF_RGX) || scanQuality.equalsIgnoreCase("UNUSABLE")) {
				continue;
			}
			if (currScanType.matches(RFMRI_RGX)) {
				hasRest = true;
				hasAny = true;
			} else if (currSeriesDesc.matches(CARIT_RGX)) {
				hasCarit = true;
				hasAny = true;
			} else if (currSeriesDesc.matches(VISMOTOR_RGX)) {
				hasVismotor = true;
				hasAny = true;
			} else if (currSeriesDesc.matches(GUESSING_RGX)) {
				hasGuessing = true;
				hasAny = true;
			} else if (currSeriesDesc.matches(EMOTION_RGX)) {
				hasEmotion = true;
				hasAny = true;
			} else if (currSeriesDesc.matches(FACENAME_RGX)) {
				hasFacename = true;
				hasAny = true;
			} else if (currSeriesDesc.matches(PCASL_RGX)) {
				hasPcasl = true;
				hasAny = true;
			}
		}
		if (!hasAny) {
			return;
		}
		
        final SanityCheck sCheck = new SanityCheck(imageSession);
        sCheck.setOutcome(Outcome.PASSED);
        sCheck.setCheckShortName(HAS_LINKEDDATA_CHECK_SHORT_NAME);
        sCheck.setCheckLongName(HAS_LINKEDDATA_CHECK_ALT_LONG_NAME);
        final XnatResourcecatalog linkedDataResource = ResourceUtils.getResource(imageSession, CommonConstants.LINKED_DATA_RESOURCE);
        if (linkedDataResource == null) {
        	sCheck.setOutcome(Outcome.WARNING);
        	sCheck.setDescriptionOfFailure("Linked data not uploaded");
        	checkList.add(sCheck);
        	// To be consistent with earlier automation-based checks, let's just skip individual file checks
        	// when we don't have a linked data resource
        	return;
        } else {
        	checkList.add(sCheck);
        }
        
        if (hasRest) {
        	
	        final SanityCheck reCheck = new SanityCheck(imageSession);
	        reCheck.setOutcome(Outcome.PASSED);
	        reCheck.setCheckShortName(HAS_REST_EYECAM_SHORT_NAME);
	        reCheck.setCheckLongName(HAS_REST_EYECAM_LONG_NAME);
	        if (!hasMatchingFile(linkedDataResource, REST_EYECAM_RGX)) {
	        	reCheck.setOutcome(Outcome.WARNING);
	        	reCheck.setDescriptionOfFailure("REST EyeCamFPS file does not exist in session resource.");
	        } 
	        checkList.add(reCheck);
        	
	        final SanityCheck rpCheck = new SanityCheck(imageSession);
	        rpCheck.setOutcome(Outcome.PASSED);
	        rpCheck.setCheckShortName(HAS_SESS_PSYDAT_REST_SHORT_NAME);
	        rpCheck.setCheckLongName(HAS_SESS_PSYDAT_REST_LONG_NAME);
	        if (!(hasMatchingFile(linkedDataResource, REST_PSYDAT_RGX) || hasMatchingFile(linkedDataResource, REST_LOG_RGX))) {
	        	rpCheck.setOutcome(Outcome.WARNING);
	        	rpCheck.setDescriptionOfFailure("REST psydat and/or log file does not exist in session resource.");
	        } 
	        checkList.add(rpCheck);
        	
        }
        if (hasCarit) {
        	
	        final SanityCheck cpCheck = new SanityCheck(imageSession);
	        cpCheck.setOutcome(Outcome.PASSED);
	        cpCheck.setCheckShortName(HAS_SESS_PSYDAT_CARIT_SHORT_NAME);
	        cpCheck.setCheckLongName(HAS_SESS_PSYDAT_CARIT_LONG_NAME);
	        if (!(hasMatchingFile(linkedDataResource, CARIT_PSYDAT_RGX) || hasMatchingFile(linkedDataResource, CARIT_LOG_RGX))) {
	        	cpCheck.setOutcome(Outcome.FAILED);
	        	cpCheck.setDescriptionOfFailure("CARIT psydat and/or log file does not exist in session resource.");
	        } 
	        checkList.add(cpCheck);
        	
        }
        if (hasVismotor) {
        	
	        final SanityCheck cpCheck = new SanityCheck(imageSession);
	        cpCheck.setOutcome(Outcome.PASSED);
	        cpCheck.setCheckShortName(HAS_SESS_PSYDAT_VISMOTOR_SHORT_NAME);
	        cpCheck.setCheckLongName(HAS_SESS_PSYDAT_VISMOTOR_LONG_NAME);
	        if (!(hasMatchingFile(linkedDataResource, VISMOTOR_PSYDAT_RGX) || hasMatchingFile(linkedDataResource, VISMOTOR_LOG_RGX))) {
	        	cpCheck.setOutcome(Outcome.FAILED);
	        	cpCheck.setDescriptionOfFailure("VISMOTOR psydat and/or log file does not exist in session resource.");
	        } 
	        checkList.add(cpCheck);
        	
        }
        if (hasGuessing) {
        	
	        final SanityCheck cpCheck = new SanityCheck(imageSession);
	        cpCheck.setOutcome(Outcome.PASSED);
	        cpCheck.setCheckShortName(HAS_SESS_PSYDAT_GUESSING_SHORT_NAME);
	        cpCheck.setCheckLongName(HAS_SESS_PSYDAT_GUESSING_LONG_NAME);
	        if (!(hasMatchingFile(linkedDataResource, GUESSING_PSYDAT_RGX) || hasMatchingFile(linkedDataResource, GUESSING_LOG_RGX))) {
	        	cpCheck.setOutcome(Outcome.FAILED);
	        	cpCheck.setDescriptionOfFailure("GUESSING psydat and/or log file does not exist in session resource.");
	        } 
	        checkList.add(cpCheck);
        	
        }
        if (hasEmotion) {
        	
	        final SanityCheck cpCheck = new SanityCheck(imageSession);
	        cpCheck.setOutcome(Outcome.PASSED);
	        cpCheck.setCheckShortName(HAS_SESS_PSYDAT_EMOTION_SHORT_NAME);
	        cpCheck.setCheckLongName(HAS_SESS_PSYDAT_EMOTION_LONG_NAME);
	        if (!(hasMatchingFile(linkedDataResource, EMOTION_PSYDAT_RGX) || hasMatchingFile(linkedDataResource, EMOTION_LOG_RGX))) {
	        	cpCheck.setOutcome(Outcome.FAILED);
	        	cpCheck.setDescriptionOfFailure("EMOTION psydat and/or log file does not exist in session resource.");
	        } 
	        checkList.add(cpCheck);
        	
        }
        if (hasFacename) {
        	
	        final SanityCheck cpCheck = new SanityCheck(imageSession);
	        cpCheck.setOutcome(Outcome.PASSED);
	        cpCheck.setCheckShortName(HAS_SESS_PSYDAT_FACENAME_SHORT_NAME);
	        cpCheck.setCheckLongName(HAS_SESS_PSYDAT_FACENAME_LONG_NAME);
	        if (!(hasMatchingFile(linkedDataResource, FACENAME_PSYDAT_RGX) || hasMatchingFile(linkedDataResource, FACENAME_LOG_RGX))) {
	        	cpCheck.setOutcome(Outcome.FAILED);
	        	cpCheck.setDescriptionOfFailure("FACENAME psydat and/or log file does not exist in session resource.");
	        } 
	        checkList.add(cpCheck);
        	
        }
        if (hasPcasl) {
        	
	        final SanityCheck reCheck = new SanityCheck(imageSession);
	        reCheck.setOutcome(Outcome.PASSED);
	        reCheck.setCheckShortName(HAS_PCASL_EYECAM_SHORT_NAME);
	        reCheck.setCheckLongName(HAS_PCASL_EYECAM_LONG_NAME);
	        if (!hasMatchingFile(linkedDataResource, PCASL_EYECAM_RGX)) {
	        	reCheck.setOutcome(Outcome.WARNING);
	        	reCheck.setDescriptionOfFailure("PCASL EyeCamFPS file does not exist in session resource.");
	        } 
	        checkList.add(reCheck);
        	
	        final SanityCheck rpCheck = new SanityCheck(imageSession);
	        rpCheck.setOutcome(Outcome.PASSED);
	        rpCheck.setCheckShortName(HAS_SESS_PSYDAT_PCASL_SHORT_NAME);
	        rpCheck.setCheckLongName(HAS_SESS_PSYDAT_PCASL_LONG_NAME);
	        if (!(hasMatchingFile(linkedDataResource, PCASL_PSYDAT_RGX) || hasMatchingFile(linkedDataResource, PCASL_LOG_RGX))) {
	        	rpCheck.setOutcome(Outcome.WARNING);
	        	rpCheck.setDescriptionOfFailure("PCASL psydat and/or log file does not exist in session resource.");
	        } 
	        checkList.add(rpCheck);

        }
		
	}

	
	@Autowired
	public CcfAabcChecker(ValidationFailureChecker validationFailureChecker, FunctionalQcChecker functionalQcChecker, 
			FacemaskingChecker facemaskingChecker, ResourceVerificationChecker resourceVerificationChecker) {
		super(validationFailureChecker, functionalQcChecker, facemaskingChecker, resourceVerificationChecker);
		this.addSessionChecksToRemove(Arrays.asList(new String[] {"protocolVal"}));
		this.addScanChecksToRemove(Arrays.asList(new String[] {"has_facename", "has_eyecam", "nifti", PHYSIOEV_CHECK_SHORT_NAME}));
		_seriesDescNiftiCheckExclusionRegexes.add(SCOUT_RGX);
		_seriesDescNiftiCheckExclusionRegexes.add(LOCALIZER_RGX);
		_scanTypeNiftiCheckExclusionRegexes.add(SCOUT_RGX);
		_scanTypeNiftiCheckExclusionRegexes.add(LOCALIZER_RGX);
		_seriesDescFrameCountExclusionRegexes.add(LOCALIZER_RGX);
		_scanTypeFrameCountExclusionRegexes.add(LOCALIZER_RGX);
		
	}

	@Override
	public void doNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user, 
			List<SanityCheck> checkList) throws SanityChecksException {
		
		log.debug("Run CCF AABC Sanity Checks");
		
		if (!imageSession.getLabel().contains("_7T") && !imageSession.getLabel().contains("MRS")) {
			checkList.addAll(_validationFailureChecker.doChecks(projectId, imageSession, user));
		}
		checkList.addAll(_facemaskingChecker.doChecks(projectId, imageSession, user, 
				Arrays.asList(new String[] { "T1w_Norm_4e", "T1w_4e", "T1w_Norm", "T1w", "T2w", "T2w_Norm" })));
		checkList.addAll(_functionalQcChecker.doChecks(projectId, imageSession, user));
		checkList.addAll(_resourceVerificationChecker.doChecks(projectId, imageSession, user));
		
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			doScanLevelLinkedDataChecks(scan, checkList);
			doSufficientFramesCheck(scan, checkList);
			doPhysioEvChecks(scan, checkList);
		}
		
		doSessionLevelLinkedDataChecks(imageSession, checkList);
		
	}

}
