package org.nrg.ccf.sanitychecks.components.checkers.project;

import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.ResourceVerificationChecker;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.springframework.stereotype.Component;

@Component
@SanityChecker(description = "Java-based sanity checks for AABC_STG")
public class CcfAabcStagingChecker extends CcfHcaStagingChecker {
	
	public CcfAabcStagingChecker(ResourceVerificationChecker resourceVerificationChecker, CcfReleaseRulesUtils releaseRulesUtils) {
		super(resourceVerificationChecker, releaseRulesUtils);
	}

	@Override
	public void initializeSourceProjectValue() {
		SOURCE_PROJECTS = new String[] {"AABC_WU_ITK","AABC_UMN_ITK","AABC_MGH_ITK","AABC_UCLA_ITK"};
	}

}
	