package org.nrg.ccf.sanitychecks.components.checkers.project;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.components.abst.AbstractCcfStagingSanityChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.ResourceVerificationChecker;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@SanityChecker(description = "Java-based sanity checks for CCF_MDP_STG")
public class CcfMdpStagingChecker extends AbstractCcfStagingSanityChecker {
	
   	public static final String HAS_LINKEDDATA_CHECK_SHORT_NAME = "linked_data";
   	public static final String HAS_LINKEDDATA_CHECK_LONG_NAME = "Has Linked data resource?";
   	
   	public static final String HAS_EV_CHECK_SHORT_NAME = "EV_CHECK";
   	public static final String HAS_EV_CHECK_LONG_NAME = "Has EV files?";
   	
   	public static final String HAS_EYETRACKER_CHECK_SHORT_NAME = "EYETRACK_CHECK";
   	public static final String HAS_EYETRACKER_CHECK_LONG_NAME = "Has EyeTracker file?";
   	
   	public static final String HAS_TASKMAT_CHECK_SHORT_NAME = "TASKMAT_CHECK";
   	public static final String HAS_TASKMAT_CHECK_LONG_NAME = "Has Task MAT file?";
   	
	public static final String 
			TFMRI_RGX = "(?i)^tfMRI.*", 
			RFMRI_RGX = "(?i)^rfMRI.*", 
			DMRI_RGX = "(?i)^dMRI.*",
			SBREF_RGX = "(?i)^.*_SBRef$" 
			;
	
	protected List<String> MAIN_SCAN_TYPES = Arrays.asList(new String[] {"rfMRI","tfMRI","dMRI"});
	protected List<String> LINKED_DATA_REGEXES = Arrays.asList(new String[] {"^tfMRI$"});

	@Lazy
	@Autowired
	public CcfMdpStagingChecker(ResourceVerificationChecker resourceVerificationChecker, CcfReleaseRulesUtils releaseRulesUtils) {
		super(resourceVerificationChecker, releaseRulesUtils);
	}

	@Override
	public void initializeSourceProjectValue() {
		SOURCE_PROJECTS = new String[] {"CCF_MDP_PRC"};
	}

	@Override
	public void doNonStagingNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user,
			List<SanityCheck> checkList) throws SanityChecksException {
		log.debug("Run CCF MDP Staging Sanity Checks");
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			doScanLevelLinkedDataChecks(scan, checkList);
			doEvCheck(scan, checkList);
			doEyeTrackerCheck(scan, checkList);
			doTaskMatCheck(scan, checkList);
		}
	}

	@Override
	public String getSelectionCriteriaFromSession(XnatImagesessiondataI session) {
		// Currently, we're only supporting 3T
		return "3T";
	}

	@Override
	public void initializeScanTypeNiftiFileCountMap() {
        _scanTypeNiftiFileCountMap.put("FieldMap_SE_EPI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("rfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("rfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("tfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("tfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("dMRI", new Integer[] {4});
	// 2023/01/02:  dMRI SBRefs now sometimes have bvec/bval files.  Okaying "4" as a valid count, in addition to "2"
	// Per MH, it's okay for SBRefs to have bvec/bval files.
        _scanTypeNiftiFileCountMap.put("dMRI_SBRef", new Integer[] {2,4});
	}

	@Override
	public void initializeSeriesDescNiftiFileCountMap() { }
	

	protected void doScanLevelLinkedDataChecks(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		final String scanQuality = scan.getQuality();
        final String currScanType = scan.getType();
        boolean isLinkedDataScan = false;
        for (final String regex : LINKED_DATA_REGEXES) {
        	if (currScanType.matches(regex)) {
        		isLinkedDataScan = true;
        	}
		}
		if ((!isLinkedDataScan) || currScanType.matches(SBREF_RGX) || scanQuality.equalsIgnoreCase("UNUSABLE")) {
			return;
		}
        final SanityCheck sCheck = new SanityCheck(scan);
        sCheck.setOutcome(Outcome.PASSED);
        sCheck.setCheckShortName(HAS_LINKEDDATA_CHECK_SHORT_NAME);
        sCheck.setCheckLongName(HAS_LINKEDDATA_CHECK_LONG_NAME);
        final XnatResourcecatalog linkedDataResource = ResourceUtils.getResource(scan, CommonConstants.LINKED_DATA_RESOURCE);
        if (linkedDataResource == null) {
        	sCheck.setOutcome(Outcome.FAILED);
        	sCheck.setDescriptionOfFailure("Linked data not uploaded");
        	checkList.add(sCheck);
        	// To be consistent with earlier automation-based checks, let's just skip individual file checks
        	// when we don't have a linked data resource
        	return;
        } else { 
        	checkList.add(sCheck);
        }
        
    } 
	
	protected void doEvCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		final String scanType = scan.getType();
		final String scanQuality = scan.getQuality();
        final String currSeriesDesc = scan.getSeriesDescription();
       	if (!scanType.matches(TFMRI_RGX) || scanType.matches(SBREF_RGX)) {
			return;
       	}
       	// MOVIE AND RETINO TASK SCANS DON'T HAVE EV
       	if (currSeriesDesc.contains("MOVIE") || currSeriesDesc.contains("RETINO")) {
       		return;
       		
       	}
		if (scanQuality.equalsIgnoreCase("UNUSABLE")) {
			return;
		}
        SanityCheck sCheck = new SanityCheck(scan);
        sCheck.setOutcome(Outcome.PASSED);
        sCheck.setCheckLongName(HAS_EV_CHECK_LONG_NAME);
        sCheck.setCheckShortName(HAS_EV_CHECK_SHORT_NAME);
        XnatResourcecatalog linkedDataResource = null;
        final StringBuilder probSB = new StringBuilder();
        if (currSeriesDesc.matches(TFMRI_RGX)) { 
        	if (!hasEv(scan,linkedDataResource)) {
        		if (!probSB.toString().isEmpty()) {
        			probSB.append(",");
        		}
        		probSB.append("Missing EV file(s)");
        		sCheck.setOutcome(Outcome.FAILED);
        	}
        }
        if (sCheck.getOutcome().equals(Outcome.FAILED)) {
        	sCheck.setDescriptionOfFailure(probSB.toString());
        }
       	checkList.add(sCheck);
    } 

	private void doEyeTrackerCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {
		final String scanType = scan.getType();
		final String scanQuality = scan.getQuality();
        final String currSeriesDesc = scan.getSeriesDescription();
       	if (!(scanType.matches(TFMRI_RGX) || scanType.matches(RFMRI_RGX)) || scanType.matches(SBREF_RGX)) {
			return;
       	}
       	// Let's not check some scans
       	if (scanType.matches(TFMRI_RGX)) { 
       		if (currSeriesDesc.toUpperCase().contains("EMOTION") || currSeriesDesc.toUpperCase().contains("FULLFIELD") || 
       				currSeriesDesc.toUpperCase().contains("GAMBLING")) {
       			return;
       		}
       	}
		if (scanQuality.equalsIgnoreCase("UNUSABLE")) {
			return;
		}
        SanityCheck sCheck = new SanityCheck(scan);
        sCheck.setOutcome(Outcome.PASSED);
        sCheck.setCheckLongName(HAS_EYETRACKER_CHECK_LONG_NAME);
        sCheck.setCheckShortName(HAS_EYETRACKER_CHECK_SHORT_NAME);
        XnatResourcecatalog linkedDataResource = null;
        final StringBuilder probSB = new StringBuilder();
        if (currSeriesDesc.matches(TFMRI_RGX)) { 
        	if (!hasEyeTracker(scan,linkedDataResource)) {
        		if (!probSB.toString().isEmpty()) {
        			probSB.append(",");
        		}
        		probSB.append("Missing EyeTracker file(s)");
        		sCheck.setOutcome(Outcome.FAILED);
        	}
        }
        if (sCheck.getOutcome().equals(Outcome.FAILED)) {
        	sCheck.setDescriptionOfFailure(probSB.toString());
        }
       	checkList.add(sCheck);
		
	}

	private void doTaskMatCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {
		final String scanType = scan.getType();
		final String scanQuality = scan.getQuality();
        final String currSeriesDesc = scan.getSeriesDescription();
       	if (!scanType.matches(TFMRI_RGX) || scanType.matches(SBREF_RGX)) {
			return;
       	}
       	// Only MOVIE, RETINO and FullField scans normally have these
       	if (!(currSeriesDesc.contains("MOVIE") || currSeriesDesc.contains("RETINO") ||
       			currSeriesDesc.toUpperCase().contains("FULLFIELD"))) {
       		return;
       		
       	}
		if (scanQuality.equalsIgnoreCase("UNUSABLE")) {
			return;
		}
        SanityCheck sCheck = new SanityCheck(scan);
        sCheck.setOutcome(Outcome.PASSED);
        sCheck.setCheckLongName(HAS_EV_CHECK_LONG_NAME);
        sCheck.setCheckShortName(HAS_EV_CHECK_SHORT_NAME);
        XnatResourcecatalog linkedDataResource = null;
        final StringBuilder probSB = new StringBuilder();
        if (currSeriesDesc.matches(TFMRI_RGX)) { 
        	if (!hasTaskMat(scan,linkedDataResource)) {
        		if (!probSB.toString().isEmpty()) {
        			probSB.append(",");
        		}
        		probSB.append("Missing Task .cwmat file(s)");
        		sCheck.setOutcome(Outcome.FAILED);
        	}
        }
        if (sCheck.getOutcome().equals(Outcome.FAILED)) {
        	sCheck.setDescriptionOfFailure(probSB.toString());
        }
       	checkList.add(sCheck);
		
	}
	
	private boolean hasEv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("/EVs/")) {
				return true;
			}
		}
		return false;
	}

	private boolean hasEyeTracker(XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("/EYE_TRACKER/")) {
				return true;
			}
		}
		return false;
	}

	private boolean hasTaskMat(XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("/TASK_TIMING/") && fp.endsWith(".mat")) {
				return true;
			}
		}
		return false;
	}
	
	private XnatResourcecatalog initializeLinkedDataResource(final XnatImagescandataI scan) {
		for (final XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			if (resource.getLabel().equals(CommonConstants.LINKED_DATA_RESOURCE)) {
				return (XnatResourcecatalog)resource;
			}
		}
		return null;
	}

}
	
