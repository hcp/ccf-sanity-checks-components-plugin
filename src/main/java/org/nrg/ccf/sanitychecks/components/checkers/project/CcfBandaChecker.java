package org.nrg.ccf.sanitychecks.components.checkers.project;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sanitychecks.abst.AbstractNiftiCheckerSanityChecker;
import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class CcfBandaChecker to execute sanity checks for CCF_BANDA_PRC project.
 * @author Atul
 */

@Slf4j
@Lazy
@Component
@Primary
@SanityChecker(description = "Java-based sanity checks for CCF_BANDA_PRC")
public class CcfBandaChecker extends AbstractNiftiCheckerSanityChecker {

	/** The Constant EV_CHECK_SHORT_NAME. */
	public static final String EV_CHECK_SHORT_NAME = "EV_CHECK";
	
	/** The Constant EV_CHECK_LONG_NAME. */
	public static final String EV_CHECK_LONG_NAME = "EV files generated (PSYCHOPY/EVs folder)?";

	/** The Constant TSV_CHECK_SHORT_NAME. */
	public static final String TSV_CHECK_SHORT_NAME = "TSV_CHECK";
	
	/** The Constant TSV_CHECK_LONG_NAME. */
	public static final String TSV_CHECK_LONG_NAME = "TSV files generated (PSYCHOPY/TSV folder)?";

	/** The Constant RESTCSV_CHECK_SHORT_NAME. */
	public static final String RESTCSV_CHECK_SHORT_NAME = "RESTCSV_CHECK";
	
	/** The Constant RESTCSV_CHECK_LONG_NAME. */
	public static final String RESTCSV_CHECK_LONG_NAME = "RESTCSV files generated (PSYCHOPY folder, CSVs for each run)?";

	/** The Constant TASKCSV_CHECK_SHORT_NAME. */
	public static final String TASKCSV_CHECK_SHORT_NAME = "TASKCSV_CHECK";
	
	/** The Constant TASKCSV_CHECK_LONG_NAME. */
	public static final String TASKCSV_CHECK_LONG_NAME = "TASKCSV files generated (PSYCHOPY folder, CSVs for each run)?";

	/** The Constant SBREF_RGX. */
	public static final String 
	TFMRI_RGX = "(?i)^.*fMRI_[^r].*", 
	RFMRI_RGX = "(?i)^.*fMRI_r.*", 
	DMRI_RGX = "(?i)^.*dMRI.*",
	SBREF_RGX = "(?i)^.*_SBRef$" 
	;

	/** The main scan types. */
	private List<String> MAIN_SCAN_TYPES = Arrays.asList("rfMRI","tfMRI");
	private JdbcTemplate _jdbcTemplate;

	/**
	 * Instantiates a new ccf BANDA checker.
	 */
	@Autowired
	@Lazy
	public CcfBandaChecker(JdbcTemplate jdbcTemplate) {
		super();
		_jdbcTemplate = jdbcTemplate;
	}

	@Override
	public void initializeScanTypeNiftiFileCountMap() {
        _scanTypeNiftiFileCountMap.put("AAHScout", new Integer[] {2, 6, 8});
        _scanTypeNiftiFileCountMap.put("Localizer", new Integer[] {6});
        _scanTypeNiftiFileCountMap.put("T1w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_setter", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_setter", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("rfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("rfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("tfMRI", new Integer[] {2, 3});
        _scanTypeNiftiFileCountMap.put("tfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("dMRI", new Integer[] {4});
	// 2023/01/02:  dMRI SBRefs now sometimes have bvec/bval files.  Okaying "4" as a valid count, in addition to "2"
	// Per MH, it's okay for SBRefs to have bvec/bval files.
        _scanTypeNiftiFileCountMap.put("dMRI_SBRef", new Integer[] {2,4});
	}
	
	@Override
	protected void initializeInitialFramesMaps(String projectId) {
		super.initializeInitialFramesMaps(projectId);
		if (_initialFramesDescMap.get(projectId).isEmpty() && projectId.toUpperCase().contains("BANDA")) {
			final List<String> bandaList = _jdbcTemplate.queryForList(
					"SELECT DISTINCT b.series_description FROM xnat_experimentdata a, xnat_imagescandata b " +
					  "WHERE a.project='CCF_BANDA_PRC' AND a.id=b.image_session_id AND " + 
					      "LOWER(b.series_description) SIMILAR TO '%(_conflic|_gamb|_rest)%' AND " +
					      "LOWER(b.series_description) NOT LIKE '%_sbref';"
					, String.class); 
			_initialFramesDescMap.get(projectId).addAll(bandaList);
		}
	}

	
	/* (non-Javadoc)
	 * @see org.nrg.ccf.sanitychecks.inter.SanityCheckerI#doChecks(java.lang.String, org.nrg.xdat.model.XnatImagesessiondataI, org.nrg.xft.security.UserI)
	 */
	@Override
	public void doNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user, List<SanityCheck> checkList) {

		log.debug("Run CCF BANDA Non-NIFTI Sanity Checks");

		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			final String scanType = scan.getType();
			if ((scanType != null && !MAIN_SCAN_TYPES.contains(scanType))) {
				continue;
			}
			doEvCheck(scan, checkList);
			doTsvCheck(scan, checkList);
			doRestCsvCheck(scan, checkList);
			doTaskCsvCheck(scan, checkList);
		}

	}
	
	/**
	 * Do rfMRI CSV check.
	 *
	 * @param scan the scan
	 * @param checkList the check list
	 */
	protected void doRestCsvCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {

		final String currSeriesDesc = scan.getSeriesDescription();
		if (currSeriesDesc == null || !currSeriesDesc.matches(RFMRI_RGX)) { 
			return;
		}
		final SanityCheck sCheck = new SanityCheck(scan);
		sCheck.setOutcome(Outcome.PASSED);
		sCheck.setCheckLongName(RESTCSV_CHECK_LONG_NAME);
		sCheck.setCheckShortName(RESTCSV_CHECK_SHORT_NAME);
		XnatResourcecatalog linkedDataResource = null;
		final StringBuilder probSB = new StringBuilder();
		final String scanQuality = scan.getQuality();
		if (!(scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE"))) {
			if (!hasRestCsv(scan,linkedDataResource)) {
				if (!probSB.toString().isEmpty()) {
					probSB.append(",");
				}
				probSB.append("Missing Resting State CSV file for run");
				sCheck.setOutcome(Outcome.FAILED);
			}
		} else {
			sCheck.setOutcome(Outcome.SCAN_UNUSABLE);
		}
		if (sCheck.getOutcome().equals(Outcome.FAILED)) {
			sCheck.setDescriptionOfFailure(probSB.toString());
		}
		checkList.add(sCheck);
	} 
	
	/**
	 * Do task CSV check.
	 *
	 * @param scan the scan
	 * @param checkList the check list
	 */
	protected void doTaskCsvCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {

		final String currSeriesDesc = scan.getSeriesDescription();
		if (currSeriesDesc == null || !currSeriesDesc.matches(TFMRI_RGX)) { 
			return;
		}
		final SanityCheck sCheck = new SanityCheck(scan);
		sCheck.setOutcome(Outcome.PASSED);
		sCheck.setCheckLongName(TASKCSV_CHECK_LONG_NAME);
		sCheck.setCheckShortName(TASKCSV_CHECK_SHORT_NAME);
		XnatResourcecatalog linkedDataResource = null;
		final StringBuilder probSB = new StringBuilder();
		final String scanQuality = scan.getQuality();
		if (!(scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE"))) {
			if (!hasTaskCsv(scan,linkedDataResource)) {
				if (!probSB.toString().isEmpty()) {
					probSB.append(",");
				}
				probSB.append("Missing Task CSV file for run");
				sCheck.setOutcome(Outcome.FAILED);
			}
		} else {
			sCheck.setOutcome(Outcome.SCAN_UNUSABLE);
		}
		if (sCheck.getOutcome().equals(Outcome.FAILED)) {
			sCheck.setDescriptionOfFailure(probSB.toString());
		}
		checkList.add(sCheck);
	} 
	
	/**
	 * Do ev check.
	 *
	 * @param scan the scan
	 * @param checkList the check list
	 */
	protected void doTsvCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {

		final String currSeriesDesc = scan.getSeriesDescription();
		if (currSeriesDesc == null || !currSeriesDesc.matches(TFMRI_RGX)) { 
			return;
		}
		final SanityCheck sCheck = new SanityCheck(scan);
		sCheck.setOutcome(Outcome.PASSED);
		sCheck.setCheckLongName(TSV_CHECK_LONG_NAME);
		sCheck.setCheckShortName(TSV_CHECK_SHORT_NAME);
		XnatResourcecatalog linkedDataResource = null;
		final StringBuilder probSB = new StringBuilder();
		final String scanQuality = scan.getQuality();
		if (!(scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE"))) {
			if (!hasTsv(scan,linkedDataResource)) {
				if (!probSB.toString().isEmpty()) {
					probSB.append(",");
				}
				probSB.append("Missing TSV file");
				sCheck.setOutcome(Outcome.FAILED);
			}
		} else {
			sCheck.setOutcome(Outcome.SCAN_UNUSABLE);
		}
		if (sCheck.getOutcome().equals(Outcome.FAILED)) {
			sCheck.setDescriptionOfFailure(probSB.toString());
		}
		checkList.add(sCheck);
	} 
	
	/**
	 * Do ev check.
	 *
	 * @param scan the scan
	 * @param checkList the check list
	 */
	protected void doEvCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {

		final String currSeriesDesc = scan.getSeriesDescription();
		if (currSeriesDesc == null || !currSeriesDesc.matches(TFMRI_RGX)) { 
			return;
		}
		final SanityCheck sCheck = new SanityCheck(scan);
		sCheck.setOutcome(Outcome.PASSED);
		sCheck.setCheckLongName(EV_CHECK_LONG_NAME);
		sCheck.setCheckShortName(EV_CHECK_SHORT_NAME);
		XnatResourcecatalog linkedDataResource = null;
		final StringBuilder probSB = new StringBuilder();
		final String scanQuality = scan.getQuality();
		if (!(scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE"))) {
			if (!hasEv(scan,linkedDataResource)) {
				if (!probSB.toString().isEmpty()) {
					probSB.append(",");
				}
				probSB.append("Missing EV file(s)");
				sCheck.setOutcome(Outcome.FAILED);
			}
		} else {
			sCheck.setOutcome(Outcome.SCAN_UNUSABLE);
		}
		if (sCheck.getOutcome().equals(Outcome.FAILED)) {
			sCheck.setDescriptionOfFailure(probSB.toString());
		}
		checkList.add(sCheck);
	} 


	/**
	 * Checks for wide csv and ev.
	 *
	 * @param scan the scan
	 * @param linkedDataResource the linked data resource
	 * @return true, if successful
	 */
	private boolean hasEv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}

		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("PSYCHOPY/EVs/") && fp.contains(".txt")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks for wide csv and ev.
	 *
	 * @param scan the scan
	 * @param linkedDataResource the linked data resource
	 * @return true, if successful
	 */
	private boolean hasTsv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}

		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("PSYCHOPY/TSV/") && fp.toLowerCase().endsWith(".tsv")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks for resting state csv 
	 *
	 * @param scan the scan
	 * @param linkedDataResource the linked data resource
	 * @return true, if successful
	 */
	private boolean hasRestCsv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("PSYCHOPY/") && fp.contains("Resting_Run") && fp.toLowerCase().endsWith(".csv")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks for task csv 
	 *
	 * @param scan the scan
	 * @param linkedDataResource the linked data resource
	 * @return true, if successful
	 */
	private boolean hasTaskCsv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("PSYCHOPY/") && fp.contains("_task-") && fp.toLowerCase().endsWith(".csv")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Initialize linked data resource.
	 *
	 * @param scan the scan
	 * @return the xnat resourcecatalog
	 */
	private XnatResourcecatalog initializeLinkedDataResource(final XnatImagescandataI scan) {
		for (final XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			if (resource.getLabel().equals(CommonConstants.LINKED_DATA_RESOURCE)) {
				return (XnatResourcecatalog)resource;
			}
		}
		return null;
	}

}
