package org.nrg.ccf.sanitychecks.components.checkers.project;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.sanitychecks.abst.AbstractNiftiCheckerSanityChecker;
import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.FacemaskingChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.FunctionalQcChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.ResourceVerificationChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.ValidationFailureChecker;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@Primary
@SanityChecker(description = "Java-only (non-automation) sanity checks for CCF_HCA_ITK")
public class CcfHcaNewChecker extends AbstractNiftiCheckerSanityChecker {
	
   	public static final String COMPLETION_CHECK_SHORT_NAME = "COMPLETION_CHECK";
   	public static final String COMPLETION_CHECK_LONG_NAME = "Enough frames to release?";
   	
   	// NOTE:  This check is no longer used.  We split PHYSIO and EV checks up.
   	public static final String PHYSIOEV_CHECK_SHORT_NAME = "PHYSIO_EV_CHECK";
   	//public static final String PHYSIOEV_CHECK_LONG_NAME = "Physio/EV files generated?";
   	
   	public static final String PHYSIO_CHECK_SHORT_NAME = "PHYSIO_CHECK";
   	public static final String PHYSIO_CHECK_LONG_NAME = "Physio files generated?";
   	
   	public static final String EV_CHECK_SHORT_NAME = "EV_CHECK";
   	public static final String EV_CHECK_LONG_NAME = "EV/WIDE_CSV files generated?";
   	
   	public static final String HAS_LINKEDDATA_CHECK_SHORT_NAME = "linked_data";
   	public static final String HAS_LINKEDDATA_CHECK_LONG_NAME = "Has Linked data resource?";
   	public static final String HAS_LINKEDDATA_CHECK_ALT_LONG_NAME = "Has session-level linked data resource?";
   	
   	public static final String HAS_DESIGN_CHECK_SHORT_NAME = "has_design";
   	public static final String HAS_DESIGN_CHECK_LONG_NAME = "Has design.csv file?";
   	public static final String HAS_DESIGN_CHECK_ALT_LONG_NAME = "Has design.csv or wide.csv file?";
   	
   	public static final String HAS_WIDE_CHECK_SHORT_NAME = "has_wide";
   	public static final String HAS_WIDE_CHECK_LONG_NAME = "Has wide.csv file?";
   	
   	public static final String HAS_TS_CHECK_SHORT_NAME = "has_ts";
   	public static final String HAS_TS_CHECK_LONG_NAME = "Has *_ts.csv file?";
   	
   	public static final String HAS_MP4_CHECK_SHORT_NAME = "has_mp4";
   	public static final String HAS_MP4_CHECK_LONG_NAME = "Has mp4 file?";
   	
   	public static final String HAS_REST_EYECAM_SHORT_NAME = "has_rest_eyecam";
   	public static final String HAS_REST_EYECAM_LONG_NAME = "Has REST EyeCamFPS file?";
   	
   	public static final String HAS_SESS_PSYDAT_REST_SHORT_NAME = "has_sess_psydat_rest";
   	public static final String HAS_SESS_PSYDAT_REST_LONG_NAME = "Has REST psydat and/or log file?";
   	
   	public static final String HAS_SESS_PSYDAT_CARIT_SHORT_NAME = "has_sess_psydat_carit";
   	public static final String HAS_SESS_PSYDAT_CARIT_LONG_NAME = "Has CARIT psydat and/or log file?";
   	
   	public static final String HAS_SESS_PSYDAT_VISMOTOR_SHORT_NAME = "has_sess_psydat_vismotor";
   	public static final String HAS_SESS_PSYDAT_VISMOTOR_LONG_NAME = "Has VISMOTOR psydat and/or log file?";
   	
   	public static final String HAS_SESS_PSYDAT_GUESSING_SHORT_NAME = "has_sess_psydat_guessing";
   	public static final String HAS_SESS_PSYDAT_GUESSING_LONG_NAME = "Has GUESSING psydat and/or log file?";
   	
   	public static final String HAS_SESS_PSYDAT_EMOTION_SHORT_NAME = "has_sess_psydat_emotion";
   	public static final String HAS_SESS_PSYDAT_EMOTION_LONG_NAME = "Has EMOTION psydat and/or log file?";
   	
   	public static final String HAS_SESS_PSYDAT_FACENAME_SHORT_NAME = "has_sess_psydat_facename";
   	public static final String HAS_SESS_PSYDAT_FACENAME_LONG_NAME = "Has FACENAME psydat and/or log file?";
   	
   	public static final String HAS_PCASL_EYECAM_SHORT_NAME = "has_pcasl_eyecam";
   	public static final String HAS_PCASL_EYECAM_LONG_NAME = "Has PCASL EyeCamFPS file?";
   	
   	public static final String HAS_SESS_PSYDAT_PCASL_SHORT_NAME = "has_sess_psydat_psydat";
   	public static final String HAS_SESS_PSYDAT_PCASL_LONG_NAME = "Has PCASL psydat and/or log file?";
   	
	public static final String 
			TFMRI_RGX = "(?i)^tfMRI.*", 
			RFMRI_RGX = "(?i)^rfMRI.*", 
			DMRI_RGX = "(?i)^dMRI.*",
			PCASL_RGX = "(?i)^.*PCASL.*",
			SBREF_RGX = "(?i)^.*_SBRef$", 
			CARIT_RGX = "(?i)^.*CARIT.*$", 
			GUESSING_RGX = "(?i)^.*GUESSING.*$", 
			EMOTION_RGX = "(?i)^.*EMOTION.*$", 
			VISMOTOR_RGX = "(?i)^.*VISMOTOR.*$", 
			FACENAME_RGX = "(?i)^.*FACENAME.*$", 
			WIDE_RGX = "(?i)^.*_wide.csv$", 
			DESIGN_RGX = "(?i)^.*_design.csv$", 
			MP4_RGX = "(?i)^.*[.]mp4.*$", 
			EYECAM_RGX = "(?i)^.*EyeCam.*$", 
			REST_LOG_RGX = "(?i)^.*REST.*log.*$", 
			REST_PSYDAT_RGX = "(?i)^.*REST.*psydat.*$", 
			REST_EYECAM_RGX = "(?i)^.*REST.*EyeCam.*$", 
			CARIT_LOG_RGX = "(?i)^.*CARIT.*log.*$", 
			CARIT_PSYDAT_RGX = "(?i)^.*CARIT.*psydat.*$", 
			VISMOTOR_LOG_RGX = "(?i)^.*VISMOTOR.*log.*$", 
			VISMOTOR_PSYDAT_RGX = "(?i)^.*VISMOTOR.*psydat.*$", 
			GUESSING_LOG_RGX = "(?i)^.*GUESSING.*log.*$", 
			GUESSING_PSYDAT_RGX = "(?i)^.*GUESSING.*psydat.*$", 
			EMOTION_LOG_RGX = "(?i)^.*EMOTION.*log.*$", 
			EMOTION_PSYDAT_RGX = "(?i)^.*EMOTION.*psydat.*$", 
			FACENAME_LOG_RGX = "(?i)^.*FACENAME.*log.*$", 
			FACENAME_PSYDAT_RGX = "(?i)^.*FACENAME.*psydat.*$", 
			PCASL_LOG_RGX = "(?i)^.*PCASL.*log.*$", 
			PCASL_PSYDAT_RGX = "(?i)^.*PCASL.*psydat.*$", 
			PCASL_EYECAM_RGX = "(?i)^.*PCASL.*EyeCam.*$", 
			TS_RGX = "(?i)^.*_ts.csv$" 
			;
	
	protected List<String> MAIN_SCAN_TYPES = Arrays.asList(new String[] {"rfMRI","tfMRI","dMRI"});
	protected List<String> LINKED_DATA_REGEXES = Arrays.asList(new String[] {"^rfMRI$","^tfMRI$","^.*PCASL.*$"});
	
	protected final ValidationFailureChecker _validationFailureChecker;
	protected final FunctionalQcChecker _functionalQcChecker;
	protected final FacemaskingChecker _facemaskingChecker;
	protected final ResourceVerificationChecker _resourceVerificationChecker; 
	@Override
	public void initializeScanTypeNiftiFileCountMap() {
        _scanTypeNiftiFileCountMap.put("AAHScout", new Integer[] {2, 6, 8});
        _scanTypeNiftiFileCountMap.put("FieldMap_SE_EPI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("Localizer", new Integer[] {6});
        _scanTypeNiftiFileCountMap.put("Localizer_aligned", new Integer[] {6});
        _scanTypeNiftiFileCountMap.put("T1w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_4e", new Integer[] {8});
        _scanTypeNiftiFileCountMap.put("T1w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_Norm_4e", new Integer[] {8});
        _scanTypeNiftiFileCountMap.put("T1w_setter", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_setter", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("TSE_HiResHp", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("TSE_Norm_HiResHp", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("rfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("rfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("tfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("tfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("dMRI", new Integer[] {4});
	// 2023/01/02:  dMRI SBRefs now sometimes have bvec/bval files.  Okaying "4" as a valid count, in addition to "2"
	// Per MH, it's okay for SBRefs to have bvec/bval files.
        _scanTypeNiftiFileCountMap.put("dMRI_SBRef", new Integer[] {2,4});
        _scanTypeNiftiFileCountMap.put("mbPCASLhr", new Integer[] {2});
		
	}
	
	@Autowired
	public CcfHcaNewChecker(ValidationFailureChecker validationFailureChecker, FunctionalQcChecker functionalQcChecker, 
			FacemaskingChecker facemaskingChecker, ResourceVerificationChecker resourceVerificationChecker) {
		super();
		this._validationFailureChecker = validationFailureChecker;
		this._functionalQcChecker = functionalQcChecker;
		this._facemaskingChecker = facemaskingChecker;
		this._resourceVerificationChecker = resourceVerificationChecker;
		this.addSessionChecksToRemove(Arrays.asList(new String[] {"protocolVal"}));
		this.addScanChecksToRemove(Arrays.asList(new String[] {"has_facename", "has_eyecam", "nifti", PHYSIOEV_CHECK_SHORT_NAME}));
	}

	@Override
	public void doNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user, 
			List<SanityCheck> checkList) throws SanityChecksException {
		
		log.debug("Run CCF HCA Sanity Checks");
		
		checkList.addAll(_validationFailureChecker.doChecks(projectId, imageSession, user));
		checkList.addAll(_facemaskingChecker.doChecks(projectId, imageSession, user, 
				Arrays.asList(new String[] { "T1w_Norm_4e", "T1w_4e", "T1w_Norm", "T1w", "T2w", "T2w_Norm" })));
		checkList.addAll(_functionalQcChecker.doChecks(projectId, imageSession, user));
		checkList.addAll(_resourceVerificationChecker.doChecks(projectId, imageSession, user));
		
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			doScanLevelLinkedDataChecks(scan, checkList);
			doSufficientFramesCheck(scan, checkList);
			doPhysioEvChecks(scan, checkList);
		}
		
		doSessionLevelLinkedDataChecks(imageSession, checkList);
		
	}

	protected void doSessionLevelLinkedDataChecks(XnatImagesessiondataI imageSession, List<SanityCheck> checkList) {
		
		boolean hasRest = false,
				hasCarit = false,
				hasVismotor = false,
				hasGuessing = false,
				hasEmotion = false,
				hasFacename = false,
				hasPcasl = false,
				hasAny = false
		;
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			final String currSeriesDesc = scan.getSeriesDescription();
			final String currScanType = scan.getType();
			final String scanQuality = scan.getQuality();
			if (!(MAIN_SCAN_TYPES.contains(currScanType)) || currScanType.matches(SBREF_RGX) || scanQuality.equalsIgnoreCase("UNUSABLE")) {
				continue;
			}
			if (currScanType.matches(RFMRI_RGX)) {
				hasRest = true;
				hasAny = true;
			} else if (currSeriesDesc.matches(CARIT_RGX)) {
				hasCarit = true;
				hasAny = true;
			} else if (currSeriesDesc.matches(VISMOTOR_RGX)) {
				hasVismotor = true;
				hasAny = true;
			} else if (currSeriesDesc.matches(GUESSING_RGX)) {
				hasGuessing = true;
				hasAny = true;
			} else if (currSeriesDesc.matches(EMOTION_RGX)) {
				hasEmotion = true;
				hasAny = true;
			} else if (currSeriesDesc.matches(FACENAME_RGX)) {
				hasFacename = true;
				hasAny = true;
			} else if (currSeriesDesc.matches(PCASL_RGX)) {
				hasPcasl = true;
				hasAny = true;
			}
		}
		if (!hasAny) {
			return;
		}
		
        final SanityCheck sCheck = new SanityCheck(imageSession);
        sCheck.setOutcome(Outcome.PASSED);
        sCheck.setCheckShortName(HAS_LINKEDDATA_CHECK_SHORT_NAME);
        sCheck.setCheckLongName(HAS_LINKEDDATA_CHECK_ALT_LONG_NAME);
        final XnatResourcecatalog linkedDataResource = ResourceUtils.getResource(imageSession, CommonConstants.LINKED_DATA_RESOURCE);
        if (linkedDataResource == null) {
        	sCheck.setOutcome(Outcome.FAILED);
        	sCheck.setDescriptionOfFailure("Linked data not uploaded");
        	checkList.add(sCheck);
        	// To be consistent with earlier automation-based checks, let's just skip individual file checks
        	// when we don't have a linked data resource
        	return;
        } else {
        	checkList.add(sCheck);
        }
        
        if (hasRest) {
        	
	        final SanityCheck reCheck = new SanityCheck(imageSession);
	        reCheck.setOutcome(Outcome.PASSED);
	        reCheck.setCheckShortName(HAS_REST_EYECAM_SHORT_NAME);
	        reCheck.setCheckLongName(HAS_REST_EYECAM_LONG_NAME);
	        if (!hasMatchingFile(linkedDataResource, REST_EYECAM_RGX)) {
	        	reCheck.setOutcome(Outcome.WARNING);
	        	reCheck.setDescriptionOfFailure("REST EyeCamFPS file does not exist in session resource.");
	        } 
	        checkList.add(reCheck);
        	
	        final SanityCheck rpCheck = new SanityCheck(imageSession);
	        rpCheck.setOutcome(Outcome.PASSED);
	        rpCheck.setCheckShortName(HAS_SESS_PSYDAT_REST_SHORT_NAME);
	        rpCheck.setCheckLongName(HAS_SESS_PSYDAT_REST_LONG_NAME);
	        if (!(hasMatchingFile(linkedDataResource, REST_PSYDAT_RGX) || hasMatchingFile(linkedDataResource, REST_LOG_RGX))) {
	        	rpCheck.setOutcome(Outcome.FAILED);
	        	rpCheck.setDescriptionOfFailure("REST psydat and/or log file does not exist in session resource.");
	        } 
	        checkList.add(rpCheck);
        	
        }
        if (hasCarit) {
        	
	        final SanityCheck cpCheck = new SanityCheck(imageSession);
	        cpCheck.setOutcome(Outcome.PASSED);
	        cpCheck.setCheckShortName(HAS_SESS_PSYDAT_CARIT_SHORT_NAME);
	        cpCheck.setCheckLongName(HAS_SESS_PSYDAT_CARIT_LONG_NAME);
	        if (!(hasMatchingFile(linkedDataResource, CARIT_PSYDAT_RGX) || hasMatchingFile(linkedDataResource, CARIT_LOG_RGX))) {
	        	cpCheck.setOutcome(Outcome.FAILED);
	        	cpCheck.setDescriptionOfFailure("CARIT psydat and/or log file does not exist in session resource.");
	        } 
	        checkList.add(cpCheck);
        	
        }
        if (hasVismotor) {
        	
	        final SanityCheck cpCheck = new SanityCheck(imageSession);
	        cpCheck.setOutcome(Outcome.PASSED);
	        cpCheck.setCheckShortName(HAS_SESS_PSYDAT_VISMOTOR_SHORT_NAME);
	        cpCheck.setCheckLongName(HAS_SESS_PSYDAT_VISMOTOR_LONG_NAME);
	        if (!(hasMatchingFile(linkedDataResource, VISMOTOR_PSYDAT_RGX) || hasMatchingFile(linkedDataResource, VISMOTOR_LOG_RGX))) {
	        	cpCheck.setOutcome(Outcome.FAILED);
	        	cpCheck.setDescriptionOfFailure("VISMOTOR psydat and/or log file does not exist in session resource.");
	        } 
	        checkList.add(cpCheck);
        	
        }
        if (hasGuessing) {
        	
	        final SanityCheck cpCheck = new SanityCheck(imageSession);
	        cpCheck.setOutcome(Outcome.PASSED);
	        cpCheck.setCheckShortName(HAS_SESS_PSYDAT_GUESSING_SHORT_NAME);
	        cpCheck.setCheckLongName(HAS_SESS_PSYDAT_GUESSING_LONG_NAME);
	        if (!(hasMatchingFile(linkedDataResource, GUESSING_PSYDAT_RGX) || hasMatchingFile(linkedDataResource, GUESSING_LOG_RGX))) {
	        	cpCheck.setOutcome(Outcome.FAILED);
	        	cpCheck.setDescriptionOfFailure("GUESSING psydat and/or log file does not exist in session resource.");
	        } 
	        checkList.add(cpCheck);
        	
        }
        if (hasEmotion) {
        	
	        final SanityCheck cpCheck = new SanityCheck(imageSession);
	        cpCheck.setOutcome(Outcome.PASSED);
	        cpCheck.setCheckShortName(HAS_SESS_PSYDAT_EMOTION_SHORT_NAME);
	        cpCheck.setCheckLongName(HAS_SESS_PSYDAT_EMOTION_LONG_NAME);
	        if (!(hasMatchingFile(linkedDataResource, EMOTION_PSYDAT_RGX) || hasMatchingFile(linkedDataResource, EMOTION_LOG_RGX))) {
	        	cpCheck.setOutcome(Outcome.FAILED);
	        	cpCheck.setDescriptionOfFailure("EMOTION psydat and/or log file does not exist in session resource.");
	        } 
	        checkList.add(cpCheck);
        	
        }
        if (hasFacename) {
        	
	        final SanityCheck cpCheck = new SanityCheck(imageSession);
	        cpCheck.setOutcome(Outcome.PASSED);
	        cpCheck.setCheckShortName(HAS_SESS_PSYDAT_FACENAME_SHORT_NAME);
	        cpCheck.setCheckLongName(HAS_SESS_PSYDAT_FACENAME_LONG_NAME);
	        if (!(hasMatchingFile(linkedDataResource, FACENAME_PSYDAT_RGX) || hasMatchingFile(linkedDataResource, FACENAME_LOG_RGX))) {
	        	cpCheck.setOutcome(Outcome.FAILED);
	        	cpCheck.setDescriptionOfFailure("FACENAME psydat and/or log file does not exist in session resource.");
	        } 
	        checkList.add(cpCheck);
        	
        }
        if (hasPcasl) {
        	
	        final SanityCheck reCheck = new SanityCheck(imageSession);
	        reCheck.setOutcome(Outcome.PASSED);
	        reCheck.setCheckShortName(HAS_PCASL_EYECAM_SHORT_NAME);
	        reCheck.setCheckLongName(HAS_PCASL_EYECAM_LONG_NAME);
	        if (!hasMatchingFile(linkedDataResource, PCASL_EYECAM_RGX)) {
	        	reCheck.setOutcome(Outcome.WARNING);
	        	reCheck.setDescriptionOfFailure("PCASL EyeCamFPS file does not exist in session resource.");
	        } 
	        checkList.add(reCheck);
        	
	        final SanityCheck rpCheck = new SanityCheck(imageSession);
	        rpCheck.setOutcome(Outcome.PASSED);
	        rpCheck.setCheckShortName(HAS_SESS_PSYDAT_PCASL_SHORT_NAME);
	        rpCheck.setCheckLongName(HAS_SESS_PSYDAT_PCASL_LONG_NAME);
	        if (!(hasMatchingFile(linkedDataResource, PCASL_PSYDAT_RGX) || hasMatchingFile(linkedDataResource, PCASL_LOG_RGX))) {
	        	rpCheck.setOutcome(Outcome.FAILED);
	        	rpCheck.setDescriptionOfFailure("PCASL psydat and/or log file does not exist in session resource.");
	        } 
	        checkList.add(rpCheck);
        	
        }
		
	}

	protected void doScanLevelLinkedDataChecks(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		final String scanQuality = scan.getQuality();
        final String currSeriesDesc = scan.getSeriesDescription();
        final String currScanType = scan.getType();
        boolean isLinkedDataScan = false;
        for (final String regex : LINKED_DATA_REGEXES) {
        	if (currScanType.matches(regex)) {
        		isLinkedDataScan = true;
        	}
		}
		if ((!isLinkedDataScan) || currScanType.matches(SBREF_RGX) || scanQuality.equalsIgnoreCase("UNUSABLE")) {
			return;
		}
        final SanityCheck sCheck = new SanityCheck(scan);
        sCheck.setOutcome(Outcome.PASSED);
        sCheck.setCheckShortName(HAS_LINKEDDATA_CHECK_SHORT_NAME);
        sCheck.setCheckLongName(HAS_LINKEDDATA_CHECK_LONG_NAME);
        final XnatResourcecatalog linkedDataResource = ResourceUtils.getResource(scan, CommonConstants.LINKED_DATA_RESOURCE);
        if (linkedDataResource == null) {
        	sCheck.setOutcome(Outcome.FAILED);
        	sCheck.setDescriptionOfFailure("Linked data not uploaded");
        	checkList.add(sCheck);
        	// To be consistent with earlier automation-based checks, let's just skip individual file checks
        	// when we don't have a linked data resource
        	return;
        } else { 
        	checkList.add(sCheck);
        }
        
		if (currScanType.matches(RFMRI_RGX) || currScanType.matches(PCASL_RGX)) {
			
			final SanityCheck designCheck = new SanityCheck(scan);
			designCheck.setOutcome(Outcome.PASSED);
			designCheck.setCheckShortName(HAS_DESIGN_CHECK_SHORT_NAME);
			designCheck.setCheckLongName(HAS_DESIGN_CHECK_ALT_LONG_NAME);
			if (!(hasWide(linkedDataResource) || hasDesign(linkedDataResource))) {
				designCheck.setOutcome(Outcome.WARNING);
				designCheck.setDescriptionOfFailure("Design.csv or Wide.csv does not exist in resource.");
			}
			checkList.add(designCheck);
			
		}
		if (currSeriesDesc.matches(CARIT_RGX) || currSeriesDesc.matches(GUESSING_RGX) || currSeriesDesc.matches(EMOTION_RGX)) {
			
			final SanityCheck designCheck = new SanityCheck(scan);
			designCheck.setOutcome(Outcome.PASSED);
			designCheck.setCheckShortName(HAS_DESIGN_CHECK_SHORT_NAME);
			designCheck.setCheckLongName(HAS_DESIGN_CHECK_LONG_NAME);
			if (!(hasDesign(linkedDataResource))) {
				designCheck.setOutcome(Outcome.WARNING);
				designCheck.setDescriptionOfFailure("Design.csv does not exist in resource.");
			}
			checkList.add(designCheck);
			
		}
		if (currSeriesDesc.matches(VISMOTOR_RGX) || currSeriesDesc.matches(CARIT_RGX) || currSeriesDesc.matches(GUESSING_RGX) || 
				currSeriesDesc.matches(EMOTION_RGX) || currSeriesDesc.matches(FACENAME_RGX)) {
			
			final SanityCheck wideCheck = new SanityCheck(scan);
			wideCheck.setOutcome(Outcome.PASSED);
			wideCheck.setCheckShortName(HAS_WIDE_CHECK_SHORT_NAME);
			wideCheck.setCheckLongName(HAS_WIDE_CHECK_LONG_NAME);
			if (!(hasWide(linkedDataResource))) {
				wideCheck.setOutcome(Outcome.FAILED);
				wideCheck.setDescriptionOfFailure("Wide.csv does not exist in resource.");
			}
			checkList.add(wideCheck);
			
		}
        
		if (currScanType.matches(RFMRI_RGX) || currScanType.matches(PCASL_RGX)) {
			
			final SanityCheck mp4Check = new SanityCheck(scan);
			mp4Check.setOutcome(Outcome.PASSED);
			mp4Check.setCheckShortName(HAS_MP4_CHECK_SHORT_NAME);
			mp4Check.setCheckLongName(HAS_MP4_CHECK_LONG_NAME);
			if (!(hasMp4(linkedDataResource))) {
				mp4Check.setOutcome(Outcome.WARNING);
				mp4Check.setDescriptionOfFailure("Mp4 file does not exist in resource.");
			}
			
			checkList.add(mp4Check);
			
			final SanityCheck tsCheck = new SanityCheck(scan);
			tsCheck.setOutcome(Outcome.PASSED);
			tsCheck.setCheckShortName(HAS_TS_CHECK_SHORT_NAME);
			tsCheck.setCheckLongName(HAS_TS_CHECK_LONG_NAME);
			if (!(hasTs(linkedDataResource))) {
				tsCheck.setOutcome(Outcome.WARNING);
				tsCheck.setDescriptionOfFailure("*_ts.csv file does not exist in resource.");
			}
			
			checkList.add(tsCheck);
			
		}
        
    } 

	private boolean hasMp4(XnatResourcecatalog linkedDataResource) {
		return hasMatchingFile(linkedDataResource, MP4_RGX);
	}

	private boolean hasDesign(XnatResourcecatalog linkedDataResource) {
		return hasMatchingFile(linkedDataResource, DESIGN_RGX);
	}

	private boolean hasWide(XnatResourcecatalog linkedDataResource) {
		return hasMatchingFile(linkedDataResource, WIDE_RGX);
	}

	private boolean hasTs(XnatResourcecatalog linkedDataResource) {
		return hasMatchingFile(linkedDataResource, TS_RGX);
	}

	protected boolean hasMatchingFile(XnatResourcecatalog linkedDataResource, String rgx) {
		if (linkedDataResource == null) {
			return false;
		}
		List<String> fileNames = ResourceUtils.getFileNames(linkedDataResource);
		for (final String fName : fileNames) {
			if (fName.matches(rgx)) {
				return true;
			}
		}
		return false;
	}

	protected void doPhysioEvChecks(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		final String scanType = scan.getType();
		final String scanQuality = scan.getQuality();
        final String currSeriesDesc = scan.getSeriesDescription();
		if (!MAIN_SCAN_TYPES.contains(scanType)) {
			return;
		}
		if (scanQuality.equalsIgnoreCase("UNUSABLE")) {
			return;
		}
		
        XnatResourcecatalog linkedDataResource = null;
		
		// Currently not releasing PHYSIO for diffusion.  Let's not run a check.
        if (!scan.getType().equals("dMRI")) {
	        final SanityCheck pCheck = new SanityCheck(scan);
	        pCheck.setOutcome(Outcome.PASSED);
	        pCheck.setCheckLongName(PHYSIO_CHECK_LONG_NAME);
	        pCheck.setCheckShortName(PHYSIO_CHECK_SHORT_NAME);
	        StringBuilder probSB = new StringBuilder();
	        if (!hasPhysioCsv(scan,linkedDataResource) && !scan.getType().equals("dMRI")) {
	        	probSB.append("Missing Physio TXT file");
	        	pCheck.setOutcome(Outcome.FAILED);
	        }
	        if (pCheck.getOutcome().equals(Outcome.FAILED)) {
	        	pCheck.setDescriptionOfFailure(probSB.toString());
	        }
	       	checkList.add(pCheck);
        }
       	
        if (currSeriesDesc.matches(TFMRI_RGX)) { 
	        final SanityCheck eCheck = new SanityCheck(scan);
	        eCheck.setOutcome(Outcome.PASSED);
	        eCheck.setCheckLongName(EV_CHECK_LONG_NAME);
	        eCheck.setCheckShortName(EV_CHECK_SHORT_NAME);
	        StringBuilder probSB = new StringBuilder();
	        probSB = new StringBuilder();
	       	if (!hasWideCsvAndEv(scan,linkedDataResource)) {
	       		if (!probSB.toString().isEmpty()) {
	       			probSB.append(",");
	       		}
	       		probSB.append("Missing generated wide CSV and/or EV file(s)");
	       		eCheck.setOutcome(Outcome.FAILED);
	       	}
	        if (eCheck.getOutcome().equals(Outcome.FAILED)) {
	        	eCheck.setDescriptionOfFailure(probSB.toString());
	        }
	       	checkList.add(eCheck);
        }
       	
    } 

	protected void doSufficientFramesCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		//log.debug("Running frames check (SCAN=" + scan.getId() + ")");
        final String currSeriesDesc = scan.getSeriesDescription();
       	if (currSeriesDesc == null || currSeriesDesc.matches(SBREF_RGX) || currSeriesDesc.toUpperCase().contains("PHYSIO")) {
                return;
        }
        if (currSeriesDesc.matches(RFMRI_RGX) || currSeriesDesc.matches(TFMRI_RGX)) { 
        	
        	SanityCheck sCheck = new SanityCheck(scan);
        	sCheck.setCheckLongName(COMPLETION_CHECK_LONG_NAME);
        	sCheck.setCheckShortName(COMPLETION_CHECK_SHORT_NAME);
            if (scan.getFrames() == null) {
            	sCheck.setOutcome(Outcome.FAILED);
            	sCheck.setDescriptionOfFailure("Could not determine frame count");
            }
            int frames = scan.getFrames().intValue();
            int frames10 = frames - 10;
            //String scanComplete = null;
            String scan100 = null;
            double scanPct = 0;
            if (currSeriesDesc.matches(RFMRI_RGX)) { 
            	//scanComplete = (frames>=488) ? "Y" : "N";
           		scanPct = (double)frames10/(488-10);
            	scan100 = (frames10>=100) ? "Y" : "N";
            	if (scan100.equals("N")) {
            		sCheck.setOutcome(Outcome.FAILED);
            		sCheck.setDescriptionOfFailure("Insufficient frames (" + frames + ")");
            	} else {
            		sCheck.setOutcome(Outcome.PASSED);
            	}
            } else if (currSeriesDesc.matches(TFMRI_RGX)) { 
            	if (currSeriesDesc.contains("_VISMOTOR")) {
            		//scanComplete = (frames>=194) ? "Y" : "N";
            		scanPct = (double)frames10/(194-10);
            	} else if (currSeriesDesc.contains("_CARIT")) {
            		//scanComplete = (frames>=300) ? "Y" : "N";
            		scanPct = (double)frames10/(300-10);
            	} else if (currSeriesDesc.contains("_FACENAME")) {
            		//scanComplete = (frames>=345) ? "Y" : "N";
            		scanPct = (double)frames10/(345-10);
            	} 
            	if (scanPct<.75) {
            		sCheck.setOutcome(Outcome.FAILED);
            		sCheck.setDescriptionOfFailure("Insufficient frames (" + frames + ")");
            	} else {
            		sCheck.setOutcome(Outcome.PASSED);
            	}
            } 
            if (sCheck.getOutcome() != null) {
            	checkList.add(sCheck);
            }
        } 
        
	}
		
	private boolean hasWideCsvAndEv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		boolean hasWideCsv = false;
		boolean hasGeneratedWide = false;
		boolean hasEV = false;
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fn = f.getName().toLowerCase();
			if (fn.contains("_wide.csv")) {
				hasWideCsv = true;
				if (fn.matches("^.*_run[0-9]_wide.csv")) {
					hasGeneratedWide = true;
				}
			} else {
				final String fp = f.getAbsolutePath();
				if (fp.contains("/EVs/")) {
					hasEV = true;
				}
			}
			if (hasWideCsv && hasGeneratedWide && hasEV) {
				return true;
			}
		}
		return false;
	}

	private boolean hasPhysioCsv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (!fp.contains("/PHYSIO/")) {
				continue;
			}
			if (fp.toLowerCase().endsWith(".csv")) {
				return true;
			}
		}
		return false;
	}

	private XnatResourcecatalog initializeLinkedDataResource(final XnatImagescandataI scan) {
		for (final XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			if (resource.getLabel().equals(CommonConstants.LINKED_DATA_RESOURCE)) {
				return (XnatResourcecatalog)resource;
			}
		}
		return null;
	}

}
