package org.nrg.ccf.sanitychecks.components.checkers.project;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.components.abst.AbstractCcfStagingSanityChecker;
import org.nrg.ccf.sanitychecks.components.checkers.shared.ResourceVerificationChecker;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@SanityChecker(description = "Java-based sanity checks for CCF_EEAJ_STG")
public class CcfEeajStagingChecker extends AbstractCcfStagingSanityChecker {
	
   	public static final String HAS_LINKEDDATA_CHECK_SHORT_NAME = "linked_data";
   	public static final String HAS_LINKEDDATA_CHECK_LONG_NAME = "Has Linked data resource?";
   	
   	public static final String HAS_TASK_CHECK_SHORT_NAME = "TASK_CHECK";
   	public static final String HAS_TASK_CHECK_LONG_NAME = "Has Task files?";
   	
	public static final String 
			TFMRI_RGX = "(?i)^tfMRI.*", 
			RFMRI_RGX = "(?i)^rfMRI.*", 
			DMRI_RGX = "(?i)^dMRI.*",
			SBREF_RGX = "(?i)^.*_SBRef$" 
			;
	
	// NOTE:  task/linked data checks are reported as warnings only because missing data is currently 
	// so extensive for ACP.  If we end up getting more, we'll change to more rigorous checks.
	protected List<String> MAIN_SCAN_TYPES = Arrays.asList(new String[] {"rfMRI","tfMRI","dMRI"});
	protected List<String> LINKED_DATA_REGEXES = Arrays.asList(new String[] {});

	@Lazy
	@Autowired
	public CcfEeajStagingChecker(ResourceVerificationChecker resourceVerificationChecker, CcfReleaseRulesUtils releaseRulesUtils) {
		super(resourceVerificationChecker, releaseRulesUtils);
	}

	@Override
	public void initializeSourceProjectValue() {
		SOURCE_PROJECTS = new String[] {"CCF_EEAJ_PRC"};
	}

	@Override
	public void doNonStagingNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user,
			List<SanityCheck> checkList) throws SanityChecksException {
		log.debug("Run CCF ACP Staging Sanity Checks");
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			doScanLevelLinkedDataChecks(scan, checkList);
			doTaskCheck(scan, checkList);
		}
	}

	@Override
	public String getSelectionCriteriaFromSession(XnatImagesessiondataI session) {
		return session.getLabel();
	}

	@Override
	public void initializeScanTypeNiftiFileCountMap() {
        _scanTypeNiftiFileCountMap.put("Bias_Receive", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("FieldMap_SE_EPI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("rfMRI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("rfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("tfMRI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("tfMRI_SBRef", new Integer[] {2});
	}

	@Override
	public void initializeSeriesDescNiftiFileCountMap() { }
	

	protected void doScanLevelLinkedDataChecks(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		final String scanQuality = scan.getQuality();
        final String currScanType = scan.getType();
        final String currScanSd = scan.getSeriesDescription();
        boolean isLinkedDataScan = false;
        for (final String regex : LINKED_DATA_REGEXES) {
        	if (currScanType.matches(regex)) {
        		isLinkedDataScan = true;
        	}
		}
		if ((!isLinkedDataScan) || currScanType.matches(SBREF_RGX) || scanQuality.equalsIgnoreCase("UNUSABLE")) {
			return;
		}
		//if (currScanSd.matches("^.*(_WM|_WORKING|_EMOTION).*$")) {
		//	return;
		//}
        final SanityCheck sCheck = new SanityCheck(scan);
        sCheck.setOutcome(Outcome.PASSED);
        sCheck.setCheckShortName(HAS_LINKEDDATA_CHECK_SHORT_NAME);
        sCheck.setCheckLongName(HAS_LINKEDDATA_CHECK_LONG_NAME);
        final XnatResourcecatalog linkedDataResource = ResourceUtils.getResource(scan, CommonConstants.LINKED_DATA_RESOURCE);
        if (linkedDataResource == null) {
        	sCheck.setOutcome(Outcome.WARNING);
        	sCheck.setDescriptionOfFailure("Linked data not uploaded");
        	checkList.add(sCheck);
        	// To be consistent with earlier automation-based checks, let's just skip individual file checks
        	// when we don't have a linked data resource
        	return;
        } else { 
        	checkList.add(sCheck);
        }
        
    } 
	
	protected void doTaskCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		final String scanType = scan.getType();
		final String scanQuality = scan.getQuality();
        final String currSeriesDesc = scan.getSeriesDescription();
       	if (!scanType.matches(TFMRI_RGX) || scanType.matches(SBREF_RGX)) {
			return;
       	}
		if (scanQuality.equalsIgnoreCase("UNUSABLE")) {
			return;
		}
		if (currSeriesDesc.matches("^.*(_WM|_WORKING|_EMOTION).*$")) {
			return;
		}
        SanityCheck sCheck = new SanityCheck(scan);
        sCheck.setOutcome(Outcome.PASSED);
        sCheck.setCheckLongName(HAS_TASK_CHECK_LONG_NAME);
        sCheck.setCheckShortName(HAS_TASK_CHECK_SHORT_NAME);
        XnatResourcecatalog linkedDataResource = null;
        final StringBuilder probSB = new StringBuilder();
        if (currSeriesDesc.matches(TFMRI_RGX)) { 
        	if (!hasTask(scan,linkedDataResource)) {
        		if (!probSB.toString().isEmpty()) {
        			probSB.append(",");
        		}
        		probSB.append("Missing TASK file(s)");
        		sCheck.setOutcome(Outcome.WARNING);
        	}
        }
        if (sCheck.getOutcome().equals(Outcome.WARNING)) {
        	sCheck.setDescriptionOfFailure(probSB.toString());
        }
       	checkList.add(sCheck);
    } 
	
	private boolean hasTask(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains(".txt")) {
				return true;
			}
		}
		return false;
	}
	
	private XnatResourcecatalog initializeLinkedDataResource(final XnatImagescandataI scan) {
		for (final XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			if (resource.getLabel().equals(CommonConstants.LINKED_DATA_RESOURCE)) {
				return (XnatResourcecatalog)resource;
			}
		}
		return null;
	}

}
	
