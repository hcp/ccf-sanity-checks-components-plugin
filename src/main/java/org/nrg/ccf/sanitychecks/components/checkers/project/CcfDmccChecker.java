package org.nrg.ccf.sanitychecks.components.checkers.project;

import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.sanitychecks.abst.AbstractNiftiCheckerSanityChecker;
import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class CcfAcpChecker to execute sanity checks for CCF_DMCC_ITK project.
 * @author Atul
 */

@Slf4j
@Lazy
@Component
@Primary
@SanityChecker(description = "Java-based sanity checks for CCF_DMCC_ITK")
public class CcfDmccChecker extends AbstractNiftiCheckerSanityChecker {

	/** The Constant SBREF_RGX. */
	public static final String 
	TFMRI_RGX = "(?i)^.*fMRI_[^r].*", 
	RFMRI_RGX = "(?i)^.*fMRI_r.*", 
	DMRI_RGX = "(?i)^.*dMRI.*",
	SBREF_RGX = "(?i)^.*_SBRef$", 
	// NOTE:  Just copied this from another project.  Many may not be represented in DMCC project data.
	MISC_RGX = "(?i)^.*(OsiriX|_Report|TENSOR|CHECK|PosDisp|Physio|Localizer|AAHead|Scout).*"
	;

	/** The main scan types. */
	private List<String> MAIN_SCAN_TYPES = Arrays.asList("rfMRI","tfMRI");
	private JdbcTemplate _jdbcTemplate;

	/**
	 * Instantiates a new ccf DMCC checker.
	 */
	@Autowired
	@Lazy
	public CcfDmccChecker(JdbcTemplate jdbcTemplate) {
		super();
		_jdbcTemplate = jdbcTemplate;
		_seriesDescNiftiCheckExclusionRegexes.add(MISC_RGX);
		_scanTypeNiftiCheckExclusionRegexes.add(MISC_RGX);
		_seriesDescFrameCountExclusionRegexes.add(MISC_RGX);
		_scanTypeFrameCountExclusionRegexes.add(MISC_RGX);
	}

	@Override
	public void initializeScanTypeNiftiFileCountMap() {
        //_scanTypeNiftiFileCountMap.put("AAHScout", new Integer[] {2, 6, 8});
        _scanTypeNiftiFileCountMap.put("FieldMap_SE_EPI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("Bias_Receive", new Integer[] {2, 6, 8});
        //_scanTypeNiftiFileCountMap.put("Localizer", new Integer[] {6, 8});
        //_scanTypeNiftiFileCountMap.put("Localizer_aligned", new Integer[] {6, 8});
        _scanTypeNiftiFileCountMap.put("T1w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("rfMRI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("rfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("tfMRI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("tfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("dMRI", new Integer[] {4});
        _scanTypeNiftiFileCountMap.put("dMRI_SBRef", new Integer[] {2});
	}
	
	@Override
	protected void initializeInitialFramesMaps(String projectId) {
		super.initializeInitialFramesMaps(projectId);
	}

	
	/* (non-Javadoc)
	 * @see org.nrg.ccf.sanitychecks.inter.SanityCheckerI#doChecks(java.lang.String, org.nrg.xdat.model.XnatImagesessiondataI, org.nrg.xft.security.UserI)
	 */
	@Override
	public void doNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user, List<SanityCheck> checkList) throws SanityChecksException {

		// NONE YET!!!!
		return;
		
	}
	
	@Override
	protected String seriesDescModifier(String seriesDesc) {
		// We've slightly modified filenames to compress out multiple underscores in series descriptions.
		return seriesDesc.replace("__", "_").replace(" ", "");
	}
	
}
