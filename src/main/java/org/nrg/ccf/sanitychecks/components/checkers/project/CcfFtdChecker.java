package org.nrg.ccf.sanitychecks.components.checkers.project;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sanitychecks.abst.AbstractNiftiCheckerSanityChecker;
import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class CcfPhcpChecker to execute sanity checks for CCF_PHCP_PRC project.
 * @author Atul
 */

@Slf4j
@Lazy
@Component
@Primary
@SanityChecker(description = "Java-based sanity checks for CCF_FTD_PRC")
public class CcfFtdChecker extends AbstractNiftiCheckerSanityChecker {

	/** The Constant COMPLETION_CHECK_SHORT_NAME. */
	public static final String COMPLETION_CHECK_SHORT_NAME = "COMPLETION_CHECK";

	/** The Constant COMPLETION_CHECK_LONG_NAME. */
	public static final String COMPLETION_CHECK_LONG_NAME = "Enough frames to release?";

	/** The Constant PHYSIO_CHECK_SHORT_NAME. */
	public static final String PHYSIO_CHECK_SHORT_NAME = "PHYSIO_CHECK";
	
	/** The Constant PHYSIO_CHECK_LONG_NAME. */
	public static final String PHYSIO_CHECK_LONG_NAME = "Physio files generated?";
	
	/** The Constant HAS_PREANON_LONG_NAME. */
	public static final String HAS_NIFTI_PREANON_LONG_NAME = "Has NIFTI_PreAnon Resource?";
	
	/** The Constant HAS_PREANON_SHORT_NAME. */
	public static final String HAS_NIFTI_PREANON_SHORT_NAME = "HAS_PREANON_CHECK";
	
	/** The Constant HAS_PREANON_LONG_NAME. */
	public static final String PREANON_COUNT_LONG_NAME = "NIFTI_PreAnon count matches NIFTI";
	
	/** The Constant HAS_PREANON_SHORT_NAME. */
	public static final String PREANON_COUNT_SHORT_NAME = "PREANON_COUNT_CHECK";
	
	/** The Constant PREANON_COUNT_LONG_NAME. */
	public static final String HAS_NIFTI_PREANON = "Has NIFTI PreAnon Resource?";

	/** The Constant EV_CHECK_SHORT_NAME. */
	public static final String EV_CHECK_SHORT_NAME = "EV_CHECK";
	
	/** The Constant EV_CHECK_LONG_NAME. */
	public static final String EV_CHECK_LONG_NAME = "EV files generated/uploaded (TASK_TIMING/EVs folder)?";

	public static final String EYETRACKER_CHECK_LONG_NAME = "EYE_TRACKER files exist (EYE_TRACKER folder)?";

	/** The Constant SBREF_RGX. */
	public static final String 
	TFMRI_RGX = "(?i)((^.*tfMRI_.*))", 
	RFMRI_RGX = "(?i)^.*rfMRI_r.*", 
	DMRI_RGX = "(?i)^.*dMRI.*",
	SBREF_RGX = "(?i)^.*_SBRef$", 
	SPIRAL_RGX = "(?i)^.*SPIRAL_V20.*",
	PHOENIX_RGX = "(?i)^.*PhoenixZIP.*",
	MISC_RGX = "(?i)^.*(OsiriX|_Report|TENSOR|CHECK|PosDisp|Physio|Localizer|AAHead|Scout).*"
	;

	/** The main scan types. */
	private List<String> MAIN_SCAN_TYPES = Arrays.asList("dMRI","rfMRI","tfMRI");
	private JdbcTemplate _jdbcTemplate;

	private String NIFTI_PREANON_RESOURCE = "NIFTI_PreAnon";
	
	@Override
	public final List<SanityCheck> doChecks(String projectId, XnatImagesessiondataI imageSession, UserI user) 
			throws SanityChecksException {
		return super.doChecks(projectId,  imageSession,  user);
	}

	/**
	 * Instantiates a new ccf PHCP checker.
	 */
	@Autowired
	@Lazy
	public CcfFtdChecker(JdbcTemplate jdbcTemplate) {
		super();
		_jdbcTemplate = jdbcTemplate;

		_seriesDescNiftiCheckExclusionRegexes.add(SPIRAL_RGX);
		_scanTypeNiftiCheckExclusionRegexes.add(SPIRAL_RGX);
		_seriesDescFrameCountExclusionRegexes.add(SPIRAL_RGX);
		_scanTypeFrameCountExclusionRegexes.add(SPIRAL_RGX);

		_seriesDescNiftiCheckExclusionRegexes.add(PHOENIX_RGX);
		_scanTypeNiftiCheckExclusionRegexes.add(PHOENIX_RGX);
		_seriesDescFrameCountExclusionRegexes.add(PHOENIX_RGX);
		_scanTypeFrameCountExclusionRegexes.add(PHOENIX_RGX);

		_seriesDescNiftiCheckExclusionRegexes.add(MISC_RGX);
		_scanTypeNiftiCheckExclusionRegexes.add(MISC_RGX);
		_seriesDescFrameCountExclusionRegexes.add(MISC_RGX);
		_scanTypeFrameCountExclusionRegexes.add(MISC_RGX);
	}
	
	@Override
	public void initializeScanTypeNiftiFileCountMap() {
        //_scanTypeNiftiFileCountMap.put("Bias_Receive", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("FieldMap_SE_EPI", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T1w_setter", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_Norm", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("T2w_setter", new Integer[] {2});
        //_scanTypeNiftiFileCountMap.put("TSE_Norm_HiResHp", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("rfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("rfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("tfMRI", new Integer[] {3});
        _scanTypeNiftiFileCountMap.put("tfMRI_SBRef", new Integer[] {2});
        _scanTypeNiftiFileCountMap.put("dMRI", new Integer[] {4});
	// 2023/01/02:  dMRI SBRefs now sometimes have bvec/bval files.  Okaying "4" as a valid count, in addition to "2"
	// Per MH, it's okay for SBRefs to have bvec/bval files.
        _scanTypeNiftiFileCountMap.put("dMRI_SBRef", new Integer[] {2,4});
	}
	
	@Override
	protected void initializeInitialFramesMaps(String projectId) {
		super.initializeInitialFramesMaps(projectId);
	}

	
	@Override
	public void doNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, UserI user, List<SanityCheck> checkList) {

		log.debug("Run CCF FTD Sanity Checks");
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			final String scanType = scan.getType();
			// For FTD, most physio scans are missing files or have non-populated files.  We'll 
			// skip all physio checks for FTD.
			//for (final XnatImagescandataI scan2 : imageSession.getScans_scan()) {
			//	// Except we can't yet generate physio for diffusion
			//	if (scan.getType().matches(DMRI_RGX)) {
			//		break;
			//	}
			//	if (scan2.getSeriesDescription().equalsIgnoreCase(scan.getSeriesDescription() + "_PhysioLog")) {
			//		doPhysioCheck(scan, checkList);
			//		break;
			//	}
			//}
			doAnonymizedNiftiChecks(scan, checkList);
			if ((scanType != null && !MAIN_SCAN_TYPES.contains(scanType))) {
				continue;
			}
			doSufficientFramesCheck(scan, checkList);
			doEvCheck(scan, checkList);
		}
		
	}

	private void doAnonymizedNiftiChecks(XnatImagescandataI scan, List<SanityCheck> checkList) {

		final String  scanType = scan.getType();
		final String  seriesDesc = scan.getSeriesDescription();

		// Very rarely, we exclude a series description from NIFTI checks
		if (isScanTypeExcludedFromNiftiChecks(scanType) || isSeriesDescExcludedFromNiftiChecks(seriesDesc)) {
			return;
		}
		if (scanType == null || seriesDesc == null) {
			// Prevent NPE:  No checks
			return;
		}
		
		boolean hasNiftiPreanon = false;
		Integer niftiCount = 0;
		Integer niftiPreanonCount = 0;
		for (final XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			if (resource.getLabel().equals(NIFTI_PREANON_RESOURCE)) {
				hasNiftiPreanon = true;	
				if (resource.getFileCount() != null) {
					niftiPreanonCount = resource.getFileCount();
				}
			} else if (resource.getLabel().equals(CommonConstants.NIFTI_RESOURCE)) {
				if (resource.getFileCount() != null) {
					niftiCount = resource.getFileCount();
				}
			}
		}
		final SanityCheck sCheck = new SanityCheck(scan);
		sCheck.setOutcome(Outcome.PASSED);
		sCheck.setCheckLongName(HAS_NIFTI_PREANON_LONG_NAME);
		sCheck.setCheckShortName(HAS_NIFTI_PREANON_SHORT_NAME);
		StringBuilder probSB = new StringBuilder();
		final String scanQuality = scan.getQuality();
		if (!(scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE"))) {
			if (!hasNiftiPreanon) {
				probSB.append("Missing NIFTI_PreAnon resource");
				sCheck.setOutcome(Outcome.FAILED);
			}
		} else {
			sCheck.setOutcome(Outcome.SCAN_UNUSABLE);
		}
		if (sCheck.getOutcome().equals(Outcome.FAILED)) {
			sCheck.setDescriptionOfFailure(probSB.toString());
		}
		checkList.add(sCheck);
		final SanityCheck cCheck = new SanityCheck(scan);
		cCheck.setOutcome(Outcome.PASSED);
		cCheck.setCheckLongName(PREANON_COUNT_LONG_NAME);
		cCheck.setCheckShortName(PREANON_COUNT_SHORT_NAME);
		probSB = new StringBuilder();
		if (!(scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE"))) {
			if (!niftiPreanonCount.equals(niftiCount)) {
				probSB.append("NIFTI resource count doesn't match NIFTI_PreAnon");
				cCheck.setOutcome(Outcome.FAILED);
			} else if (niftiPreanonCount == 0) {
				probSB.append("NIFTI_PreAnon resource contains no files");
				cCheck.setOutcome(Outcome.FAILED);
			} else if (niftiCount == 0) {
				probSB.append("NIFTI resource contains no files");
				cCheck.setOutcome(Outcome.FAILED);
			}
		} else {
			cCheck.setOutcome(Outcome.SCAN_UNUSABLE);
		}
		if (cCheck.getOutcome().equals(Outcome.FAILED)) {
			cCheck.setDescriptionOfFailure(probSB.toString());
		}
		checkList.add(cCheck);

	}

	/**
	 * Do physio ev check.
	 *
	 * @param scan the scan
	 * @param checkList the check list
	 */
	protected void doPhysioCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {

		return;
		/*
		final SanityCheck sCheck = new SanityCheck(scan);
		sCheck.setOutcome(Outcome.PASSED);
		sCheck.setCheckLongName(PHYSIO_CHECK_LONG_NAME);
		sCheck.setCheckShortName(PHYSIO_CHECK_SHORT_NAME);
		XnatResourcecatalog linkedDataResource = null;
		final StringBuilder probSB = new StringBuilder();
		final String scanQuality = scan.getQuality();
		if (!(scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE"))) {
			if (!hasPhysio(scan,linkedDataResource)) {
				if (!probSB.toString().isEmpty()) {
					probSB.append(",");
				}
				probSB.append("Missing generated physio file(s)");
				sCheck.setOutcome(Outcome.FAILED);
			}
		} else {
			sCheck.setOutcome(Outcome.SCAN_UNUSABLE);
		}
		if (sCheck.getOutcome().equals(Outcome.FAILED)) {
			sCheck.setDescriptionOfFailure(probSB.toString());
		}
		checkList.add(sCheck);
		*/
	} 
	
	/**
	 * Do ev check.
	 *
	 * @param scan the scan
	 * @param checkList the check list
	 */
	protected void doEvCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {

		final String currSeriesDesc = scan.getSeriesDescription();
		if (currSeriesDesc == null || !currSeriesDesc.matches(TFMRI_RGX)) { 
			return;
		}
		// NOTE:  MOVIE AND RETINO SCANS DON'T HAVE EV files
		if (currSeriesDesc.contains("MOVIE") || currSeriesDesc.contains("RETINO")) { 
			return;
		}
		final SanityCheck sCheck = new SanityCheck(scan);
		sCheck.setOutcome(Outcome.PASSED);
		sCheck.setCheckLongName(EV_CHECK_LONG_NAME);
		sCheck.setCheckShortName(EV_CHECK_SHORT_NAME);
		XnatResourcecatalog linkedDataResource = null;
		final StringBuilder probSB = new StringBuilder();
		final String scanQuality = scan.getQuality();
		if (!(scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE"))) {
			if (!hasEv(scan,linkedDataResource)) {
				if (!probSB.toString().isEmpty()) {
					probSB.append(",");
				}
				probSB.append("Missing EV file(s)");
				sCheck.setOutcome(Outcome.FAILED);
			}
		} else {
			sCheck.setOutcome(Outcome.SCAN_UNUSABLE);
		}
		if (sCheck.getOutcome().equals(Outcome.FAILED)) {
			sCheck.setDescriptionOfFailure(probSB.toString());
		}
		checkList.add(sCheck);
	} 

	/**
	 * Checks for wide csv and ev.
	 *
	 * @param scan the scan
	 * @param linkedDataResource the linked data resource
	 * @return true, if successful
	 */
	/*
	private boolean hasPhysio(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("/PHYSIO/") && fp.toLowerCase().endsWith(".csv")) {
				return true;
			}
		}
		return false;
	}
	*/

	/**
	 * Checks for wide csv and ev.
	 *
	 * @param scan the scan
	 * @param linkedDataResource the linked data resource
	 * @return true, if successful
	 */
	private boolean hasEv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}

		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (fp.contains("TASK_TIMING/EVs/") && fp.contains(".txt")) {
				return true;
			}
		}
		return false;
	}

	protected void doSufficientFramesCheck(XnatImagescandataI scan, List<SanityCheck> checkList) {
		
		log.debug("Running frames check (SCAN=" + scan.getId() + ")");
        final String currSeriesDesc = scan.getSeriesDescription();
       	if (currSeriesDesc == null || currSeriesDesc.matches(SBREF_RGX) || currSeriesDesc.toUpperCase().contains("PHYSIO")) {
                return;
        }
        if (currSeriesDesc.matches(RFMRI_RGX) || currSeriesDesc.matches(TFMRI_RGX)) { 
        	
        	SanityCheck sCheck = new SanityCheck(scan);
        	sCheck.setCheckLongName(COMPLETION_CHECK_LONG_NAME);
        	sCheck.setCheckShortName(COMPLETION_CHECK_SHORT_NAME);


        	// TEMPORARY set failed to remind us to set up.
          	sCheck.setOutcome(Outcome.FAILED);
           	sCheck.setDescriptionOfFailure("This check has not yet been properly configured for the FTD project");



            	/*
            if (scan.getFrames() == null) {
            	sCheck.setOutcome(Outcome.FAILED);
            	sCheck.setDescriptionOfFailure("Could not determine frame count");
            }
            int frames = scan.getFrames().intValue();
            int frames10 = frames - 10;
            //String scanComplete = null;
            String scan100 = null;
            double scanPct = 0;
            if (currSeriesDesc.matches(RFMRI_RGX)) { 
            	//scanComplete = (frames>=420) ? "Y" : "N";
           		scanPct = (double)frames10/(420-10);
            	scan100 = (frames10>=100) ? "Y" : "N";
            	if (scan100.equals("N")) {
            		sCheck.setOutcome(Outcome.FAILED);
            		sCheck.setDescriptionOfFailure("Insufficient frames (" + frames + ")");
            	} else {
            		sCheck.setOutcome(Outcome.PASSED);
            	}
            } else if (currSeriesDesc.matches(TFMRI_RGX)) { 
            	if (currSeriesDesc.contains("MOVIE")) {
            		//scanComplete = (frames>=194) ? "Y" : "N";
            		scanPct = (double)frames10/(420-10);
            	} else if (currSeriesDesc.contains("RETINO")) {
            		//scanComplete = (frames>=300) ? "Y" : "N";
            		scanPct = (double)frames10/(420-10);
            	} else if (currSeriesDesc.contains("_Emotion")) {
            		//scanComplete = (frames>=345) ? "Y" : "N";
            		scanPct = (double)frames10/(176-10);
            	} else if (currSeriesDesc.contains("_FullField")) {
            		//scanComplete = (frames>=345) ? "Y" : "N";
            		scanPct = (double)frames10/(420-10);
            	} else if (currSeriesDesc.contains("_Gambl")) {
            		//scanComplete = (frames>=345) ? "Y" : "N";
            		scanPct = (double)frames10/(253-10);
            	} else if (currSeriesDesc.contains("_PRLloc")) {
            		//scanComplete = (frames>=345) ? "Y" : "N";
            		scanPct = (double)frames10/(375-10);
            	} else if (currSeriesDesc.contains("_SocCog")) {
            		//scanComplete = (frames>=345) ? "Y" : "N";
            		scanPct = (double)frames10/(274-10);
            	} else if (currSeriesDesc.contains("_WorkMem")) {
            		//scanComplete = (frames>=345) ? "Y" : "N";
            		scanPct = (double)frames10/(405-10);
            	} else if (currSeriesDesc.contains("_WM")) {
            		//scanComplete = (frames>=345) ? "Y" : "N";
            		scanPct = (double)frames10/(405-10);
            	} 
            	if (scanPct<.75) {
            		sCheck.setOutcome(Outcome.FAILED);
            		sCheck.setDescriptionOfFailure("Insufficient frames (" + frames + ")");
            	} else {
            		sCheck.setOutcome(Outcome.PASSED);
            	}
            } 
            if (sCheck.getOutcome() != null) {
            	checkList.add(sCheck);
            }
            	*/
        } 
        
	}

	/**
	 * Initialize linked data resource.
	 *
	 * @param scan the scan
	 * @return the xnat resourcecatalog
	 */
	private XnatResourcecatalog initializeLinkedDataResource(final XnatImagescandataI scan) {
		for (final XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			if (resource.getLabel().equals(CommonConstants.LINKED_DATA_RESOURCE)) {
				return (XnatResourcecatalog)resource;
			}
		}
		return null;
	}
	
}
