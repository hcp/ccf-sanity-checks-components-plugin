package org.nrg.ccf.sanitychecks.components.checkers.shared;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.ccf.sanitychecks.inter.SanityCheckerI;
import org.nrg.ccf.sanitychecks.inter.SanityCheckerRequiresInfoI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xft.security.UserI;
import org.springframework.stereotype.Component;

@Component
public class ResourceVerificationChecker implements SanityCheckerI, SanityCheckerRequiresInfoI {
	
	public static final String RESOURCE_VERIFICATION_CHECK_SHORT_NAME = "extra-resource";
	public static final String RESOURCE_VERIFICATION_CHECK_LONG_NAME = "Resource label check";
	
	public static final List<String> _defaultResourceList = 
			new ArrayList<>(Arrays.asList(new String[] { 
			        "DICOM", "DICOM_ORIG", "DEFACE_QC", "NIFTI", "SNAPSHOTS", "secondary", "LINKED_DATA"
			})); 
	
	@Override
	public List<SanityCheck> doChecks(String projectId, XnatImagesessiondataI imageSession, UserI user) {
		
		return checkResources(projectId, imageSession, user, _defaultResourceList);
		
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SanityCheck> doChecks(String projectId, XnatImagesessiondataI imageSession, UserI user, Object resourceList)
			throws SanityChecksException {
		
		List<String> projectResourceList; 
		try {
			projectResourceList = (List<String>)resourceList;
		} catch (Exception e) {
			throw new SanityChecksException("Please provide a list of resource names", e);
		}
		
		return checkResources(projectId, imageSession, user, projectResourceList);
		
	}

	private List<SanityCheck> checkResources(String projectId, XnatImagesessiondataI imageSession, UserI user,
			List<String> resourceList) {
		
		final List<SanityCheck> checkList = new ArrayList<>();
		outerLoop:
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			
			final SanityCheck scanCheck = new SanityCheck(scan);
			scanCheck.setCheckLongName(RESOURCE_VERIFICATION_CHECK_LONG_NAME);
			scanCheck.setCheckShortName(RESOURCE_VERIFICATION_CHECK_SHORT_NAME);
			
			for (final XnatAbstractresourceI resource : ResourceUtils.getResources(scan)) {
				if (!resourceList.contains(resource.getLabel())) {
					scanCheck.setOutcome(Outcome.FAILED);
					scanCheck.setDescriptionOfFailure("Unknown resource: " + resource.getLabel());
					checkList.add(scanCheck);
					continue outerLoop;
				}
			}
			
			scanCheck.setOutcome(Outcome.PASSED);
			checkList.add(scanCheck);
			
		}
		return checkList;
		
	}



}
